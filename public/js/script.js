var redireccion = '';
var preguntas = [];





$(document).ready(function() {


    $('#tab-9 a').on("click", function(){
        $('video')[0].pause();
        $('video')[1].pause();
        $('video')[2].pause();
        $('video')[3].pause();
        $('video')[4].pause();
        $('video')[5].pause();
    });

    /*
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      var $video = $(e.relatedTarget.hash).find('video');
      $video.each(function(index, video) {
        $(video).attr("src", $(video).attr("src"));
      });
    });
    */


     $('.enviar-special').attr('disabled',true);

     var material = $('#ver_material_id').val();
     if (material) {
          $('#mat_id'+material).trigger('click');
     }



     // Esconder tajetas home
     $('.continuar').show();
     $('.aprobado').hide();
     $('.inicio').hide();


     $("#content input[type=radio]:checked").each(function() {
          var pregunta_id = $(this).attr('pregunta');
          var id = $(this).attr('preg_id');
          var mensaje = $(this).attr('mensaje');
          // $(".resp"+pregunta_id).attr('disabled',true);
          var correcta = $(this).attr('correcta');
          if (correcta == 1 ) {
               $( "#respuesta"+pregunta_id ).html( '<br><div class="alert alert-info" style=" background-color: #2a952a;     color: #fff; font-size:16px; text-align: justify;">\n\
                                                            '+mensaje+'\n\
                                                       </div>');
          }else{
               $( "#respuesta"+pregunta_id ).html( '<br><div class="alert alert-info" style=" background-color: #fe0000; color: #fff; font-size:16px; text-align: justify;">\n\
                                                            '+mensaje+'\n\
                                                       </div>');
          }
          
          var correcta = $(this).attr('correcta');
           if (correcta == 1 ) {
                 $("#"+id).css('color',  'green');
           }else{
                 $("#"+id).css('color',  'red');
           }

       });


     $(".resp_especialkey").change(function() {

          var pregunta_id = $(this).attr('pregunta');
          var id = $(this).attr('preg_id');
          var mensaje = $(this).attr('mensaje');
          // $(".resp"+pregunta_id).attr('disabled',true);
          var correcta = $(this).attr('correcta');
          if (correcta == 1 ) {
               $( "#respuesta"+pregunta_id ).html( '<br><div class="alert alert-info" style=" background-color: #2a952a;     color: #fff; font-size:16px; text-align: justify;">\n\
                                                            '+mensaje+'\n\
                                                       </div>');
          }else{
               $( "#respuesta"+pregunta_id ).html( '<br><div class="alert alert-info" style=" background-color: #fe0000; color: #fff; font-size:16px; text-align: justify;">\n\
                                                            '+mensaje+'\n\
                                                       </div>');
          }


          var existe_pregunta = preguntas.includes(pregunta_id);


          if (existe_pregunta == false) {
               preguntas.push(pregunta_id);
          }else{
               $(".resp"+pregunta_id).prop( "disabled", true );

          }



          $( ".resp"+pregunta_id ).each(function( i, $this ) {
               var correcta = $($this).attr('correcta');
               var id = $($this).attr('preg_id');
               $("#"+id).css('color',  '#5555');
           });

          var correcta = $(this).attr('correcta');
           if (correcta == 1 ) {
                 $("#"+id).css('color',  'green');
           }else{
                 $("#"+id).css('color',  'red');
           }

     });



     $(".enviar-special").bind('click', function () {
          $( "#submit_special" ).trigger( "click" );
          redireccion = $(this).attr('href');
          return false;
    });


   $(".specialform").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');



        $.ajax({
             type: "POST",
             url: url,
             dataType: "json",
             data: form.serialize(), // serializes the form's elements.
             success: function(data)
             {
                  if (data.type == 'exito') {
                       window.location.href = redireccion;

                  }
             }
        });


   });

});

// Home
function ver_cursos_ocultos(type) {
     $('.tarjetas').hide();

     switch (type) {
          case 1:
               // Iniciados
               $('.inicio').show();
          break;
          case 2:
               // Sin iniciar
               $('.continuar').show();
          break;
          case 3:
               // Terminados
               $('.aprobado').show();
          break;
     }
}




var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
     console.log(n);
     // This function will display the specified tab of the form...
     var x = document.getElementsByClassName("tab");
     x[n].style.display = "block";
     //... and fix the Previous/Next buttons:
     if (n == 0) {
          document.getElementById("prevBtn").style.display = "none";
     } else {
          document.getElementById("prevBtn").style.display = "inline";
     }
     if (n == (x.length - 1)) {
          document.getElementById("nextBtn").innerHTML = "Enviar";
     } else {
          document.getElementById("nextBtn").innerHTML = "Siguiente pregunta";
     }
     //... and run a function that will display the correct step indicator:
     fixStepIndicator(n)
}

function nextPrev(n) {


     // This function will figure out which tab to display
     var x = document.getElementsByClassName("tab");
     // Exit the function if any field in the current tab is invalid:
     if (n == 1 && !validateForm()) return false;


     // Hide the current tab:
     x[currentTab].style.display = "none";
     // Increase or decrease the current tab by 1:
     currentTab = currentTab + n;
     // if you have reached the end of the form...
     if (currentTab >= x.length) {
          // ... the form gets submitted:
          // swal({
          //   title: "¿ Desea finalizar ?",
          //   text: " Se enviarán las repuestas, no podra modificarse ",
          //   type: "warning",
          //   showCancelButton: true,
          //   confirmButtonText: 'Enviar',
          //   cancelButtonText: "Cancelar",
          //   closeOnConfirm: false,
          //   closeOnCancel: false
          // }, function(resp){
          //      if (resp) {
          //           document.getElementById("regForm").submit();
          //      }else{
          //           console.log('no');
          //           swal.close();
          //           return false;
          //      }
          // });
          jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
          });


          $( "#myform" ).validate({
            rules: {
              field: {
                required: true
              }
            }
          });


          $(':radio').attr('disabled', false);
          document.getElementById("regForm").submit();
          $('#load_img').show();
          return false;
     }
     showTab(currentTab);

     // Otherwise, display the correct tab:
}

function validateForm() {
     // This function deals with validation of the form fields
     var x, y, i, valid = true;
     x = document.getElementsByClassName("tab");
     y = x[currentTab].getElementsByTagName("input");
     // A loop that checks every input field in the current tab:
     for (i = 0; i < y.length; i++) {
          // If a field is empty...

          // validart checkbox
          var inputfields = document.getElementsByName(y[i].name);
          var ar_inputflds = inputfields.length;

          var siguiente = false;
          for (var i = 0; i < ar_inputflds; i++) {
               if (inputfields[i].checked == true)
               var siguiente = true;
          }




          if (siguiente == false) {
               swal("", "Es requerida una respuesta para continuar");
               // add an "invalid" class to the field:
               y[i].className += " invalid";
               // and set the current valid status to false
               valid = false;
          }
     }
     // If the valid status is true, mark the step as finished and valid:
     // if (valid) {
     //      document.getElementsByClassName("step")[currentTab].className += " finish";
     // }
     return valid; // return the valid status
}

function fixStepIndicator(n) {
     // This function removes the "active" class of all steps...
     var i, x = document.getElementsByClassName("step");
     for (i = 0; i < x.length; i++) {
          x[i].className = x[i].className.replace(" active", "");
     }
     //... and adds the "active" class on the current step:
     x[n].className += " active";
}


function checkform () {
     $('#load_img').show();
     return true;

}
