<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Login - <?php echo $__env->yieldContent('title'); ?> </title>


     <link rel="stylesheet" href="<?php echo asset('css/vendor.css'); ?>" />
     <link rel="stylesheet" href="<?php echo asset('css/app.css'); ?>" />
     <link rel="stylesheet" href="<?php echo asset('css/estilos/estilos.css'); ?>" />

</head>
<body class="gray-bg fondo_azul">

     <!-- Wrapper-->
     <div class="middle-box text-center loginscreen animated fadeInDown" style="padding-top: 1%;">
          <div>
               <div class=" p-md">
                    <div>
                         <h1 class="logo-name"><img src="<?php echo asset('img/logologin.png'); ?>" alt="eduPlace" class="img-responsive img-login"></h1>
                    </div>

                   <h2 style="font-weight: 600; font-size: 16px; color: #fff;">Para continuar, inicia sesión</h2>
                    <div id="error"></div>
                    <form class="form-horizontal" role="form" method="POST" id="log_correo" action="<?php echo e(route('validar_correo')); ?>">
                        <div class="col-md-12 m-b">
                             <?php echo e(csrf_field()); ?>

                             <input type="email" name="correo" class="form-control" placeholder="Correo electrónico" required="" id="ip2">
                        </div>
                         <div class="col-md-12">
                              <button type="submit" class="btn btn-primary block full-width m-b ladda-button" id="ip3">Inicia sesión</button>
                         </div>
                        <div class="clearfix"></div>
                        <a class="btn btn-link" href="https://eduplace.com.mx/public/contrasena/" style="color: #fff;">
                           ¿ Olvidaste tu contraseña ?
                        </a>
                   </form>


                     <form class="form-horizontal" role="form" method="POST" id="login" action="<?php echo e(route('login')); ?>" style="display:none;">
                         <div class="col-md-12">
                              <?php echo e(csrf_field()); ?>

                               <div class="form-group<?php echo e($errors->has('correo') ? ' has-error' : ''); ?>">
                                   <input type="hidden" id="correo" name="correo" class="form-control" placeholder="Introduce tu correo electrónico" required="" value="<?php echo e(old('correo')); ?>">
                                     <?php if($errors->has('correo')): ?>
                                         <span class="help-block" style="color: #fff">
                                             <strong ><?php echo e($errors->first('correo')); ?></strong>
                                         </span>
                                     <?php endif; ?>
                              </div>
                              <h3 id="correo_sesion"></h3>
                              <input type="hidden" name="curso_id" value="<?php echo e($curso_id); ?>">
                              <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                   <input type="password" id="password " name="password" class="form-control ip2" placeholder="Contraseña" required="" >
                                   <?php if($errors->has('password')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                              </div>
                              <button type="submit" class="btn btn-primary block full-width m-b" id="ip3">Iniciar sesión</button>
                              <button type="button" class="btn btn btn-outline btn-default full-width m-b" onclick="atras()" id="ip3" >Regresar</button>
                         </div>
                         <div class="clearfix"></div>
                         <a class="btn btn-link" href="https://eduplace.com.mx/public/contrasena/" style="color: #fff;">
                             ¿ Olvidaste tu contraseña ?
                         </a>
                    </form>

                    <form class="form-horizontal not_view" role="form" method="GET" id="register" action="<?php echo e(route('register')); ?>" >
                         <input type="text" id="curso_id" name="curso_id" value="<?php echo e($curso_id); ?>">
                         <input type="text" id="correo_reg" name="correo">
                         <button type="submit" id="btn_register" name="button">csadca</button>
                    </form>
               </div>
               <br>

          </div>
     </div>
     <!-- End wrapper-->

     <script src="<?php echo asset('js/app.js'); ?>" type="text/javascript"></script>

     <?php $__env->startSection('scripts'); ?>
     <?php echo $__env->yieldSection(); ?>

</body>
</html>
