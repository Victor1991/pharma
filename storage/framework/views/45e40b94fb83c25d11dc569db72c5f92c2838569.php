<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Registro - <?php echo $__env->yieldContent('title'); ?> </title>

     <link rel="stylesheet" href="<?php echo asset('css/vendor.css'); ?>" />
     <link rel="stylesheet" href="<?php echo asset('css/app.css'); ?>" />
          <link rel="stylesheet" href="<?php echo asset('css/estilos/estilos.css'); ?>" />

</head>

<body class="gray-bg  ">

     <!-- Wrapper-->
     <div class="middle-box text-center loginscreen animated fadeInDown box-registre"  style="padding-top: 1%;">
          <div>
               <div class=" p-md ">
                    <div>
                         <h1 class="logo-name"><img src="<?php echo asset('img/logologin.png'); ?>" alt="logo" class="img-responsive img-login"></h1>
                    </div>

                    <h2 style="font-weight: 600; font-size: 16px; color: #556c98;">Crear cuenta</h2>
                    <br>
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(route('register')); ?>">
                         <?php echo e(csrf_field()); ?>

                         <input type="hidden" name="curso_id" value="<?php echo e($curso_id); ?>">

                         <div class="form-group<?php echo e($errors->has('nombre') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="nombre" placeholder="Nombre(s)*" type="text" class="form-control ip2" name="nombre" value="<?php echo e(old('nombre')); ?>" required autofocus>
                                   <?php if($errors->has('nombre')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('nombre')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('apellido_paterno') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="apellido_paterno" placeholder="Apellido Paterno*" type="text" class="form-control ip2" name="apellido_paterno" value="<?php echo e(old('apellido_paterno')); ?>" required autofocus>
                                   <?php if($errors->has('apellido_paterno')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('apellido_paterno')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('apellidos_materno') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="apellidos_materno" placeholder="Apellido Materno*" type="text" class="form-control ip2" name="apellidos_materno" value="<?php echo e(old('apellidos_materno')); ?>" required autofocus>
                                   <?php if($errors->has('apellidos_materno')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('apellidos_materno')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('sexo') ? ' has-error' : ''); ?>" >
                              <div class="col-md-12">
                                   <select class="form-control "  name="sexo" id="sexo" required autofocus>
                                        <option value="" selected disabled hidden>Sexo *</option>
                                        <option <?php echo e(( old('sexo') ==  'masculino') ? 'selected' : ''); ?> value="masculino" > Masculino</option>
                                        <option <?php echo e(( old('sexo') ==  'femenino') ? 'selected' : ''); ?> value="femenino" > Femenino </option>
                                   </select>
                                   <?php if($errors->has('sexo')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('sexo')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('edad') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <select class="form-control"  name="edad" required autofocus >
                                        <option value="" selected disabled hidden> Edad *</option>
                                        <?php for($i=15; $i < 99 ; $i++){ ?>
                                             <option <?php echo e(( old('edad') ==  $i) ? 'selected' : ''); ?> value="<?=$i;?>"><?=$i;?></option>
                                        <?php } ?>
                                   </select>
                                   <?php if($errors->has('edad')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('edad')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('pais') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <select class="form-control" name="pais" id="pais" required autofocus >
                                        <option value="" selected disabled hidden>País *</option>
                                        <?php $__currentLoopData = $pais; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pais): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e(( old('pais') == $pais['id']) ? 'selected' : ''); ?> value="<?php echo e($pais['id']); ?>" > <?php echo e($pais['nombre']); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   </select>
                                   <?php if($errors->has('pais')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('pais')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('ciudad') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="ciudad" placeholder="Ciudad " type="text" class="form-control ip2" name="ciudad" value="<?php echo e(old('ciudad')); ?>" >
                                   <?php if($errors->has('ciudad')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('ciudad')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>


                         <div class="form-group<?php echo e($errors->has('codigo_postal') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="codigo_postal" placeholder="Código Postal " type="text" class="form-control ip2" name="codigo_postal" value="<?php echo e(old('codigo_postal')); ?>">
                                   <?php if($errors->has('codigo_postal')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('codigo_postal')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('cedula_profecional') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="cedula_profecional" placeholder="Cédula Profesional* " type="text" class="form-control ip2" name="cedula_profecional" value="<?php echo e(old('cedula_profecional')); ?>" required autofocus>
                                   <?php if($errors->has('cedula_profecional')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('cedula_profecional')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('especialidad') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <select class="form-control" name="especialidad" id="especialidad" required autofocus >
                                        <option value="" selected disabled hidden>Especialidad *</option>
                                        <?php $__currentLoopData = $especialidades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $especialidad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e(( old('especialidad') == $especialidad['id']) ? 'selected' : ''); ?> value="<?php echo e($especialidad['id']); ?>" > <?php echo e($especialidad['nombre']); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   </select>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('institucion') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <!-- <input id="institucion" placeholder="Institución* " type="text" class="form-control ip2" name="institucion" value="<?php echo e(old('institucion')); ?>" required autofocus> -->

                                   <select class="form-control" name="institucion" id="institucion" required autofocus >
                                        <option value="" selected disabled hidden> Institución* </option>
                                        <?php $__currentLoopData = $instituciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $instituciones): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e(( old('institucion') == $instituciones['id']) ? 'selected' : ''); ?> value="<?php echo e($instituciones['id']); ?>" > <?php echo e($instituciones['nombre']); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   </select>
                                   <?php if($errors->has('institucion')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('institucion')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('residentes') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <select class="form-control" name="residentes" id="residentes"  autofocus >
                                        <option value="" selected disabled hidden> Solo para residentes </option>
                                        <?php $__currentLoopData = $residentes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residentes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e(( old('residentes') == $residentes['id']) ? 'selected' : ''); ?> value="<?php echo e($residentes['id']); ?>" > <?php echo e($residentes['nombre']); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   </select>
                                   <?php if($errors->has('pais')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('pais')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('celular') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="celular" placeholder="Número de celular* " type="text" class="form-control ip2" name="celular" value="<?php echo e(old('celular')); ?>" required autofocus>
                                   <?php if($errors->has('celular')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('celular')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div>
                              <div class="form-group<?php echo e($errors->has('correo') ? ' has-error' : ''); ?>">
                                   <div class="col-md-12">
                                        <input id="correo" placeholder="Correo* " type="text" class="form-control ip2" name="correo" value="<?php echo e(isset($correo) ? $correo : old('correo')); ?>" required autofocus>
                                        <?php if($errors->has('correo')): ?>
                                        <span class="help-block">
                                             <strong><?php echo e($errors->first('correo')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <div class="col-md-12">
                                  <input id="correo-confirm" type="text" class="form-control ip2" placeholder="Confirme correo*"  name="correo_confirmation" required autofocus>
                              </div>
                         </div>


                         <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                              <div class="col-md-12">
                                   <input id="password" type="password" class="form-control ip2" name="password" placeholder="Contraseña" required autofocus>
                                   <?php if($errors->has('password')): ?>
                                   <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                   </span>
                                   <?php endif; ?>
                              </div>
                         </div>

                         <div class="form-group">

                              <div class="col-md-12">
                                  <input id="password-confirm" type="password" class="form-control ip2" placeholder="Confirme Contraseña*" name="password_confirmation" required autofocus><br>
                                  <span style="font-size: 11px; text-align: left;">(*) Campos obligatorios</span>
                              </div>
                         </div>

                         <div class="form-group<?php echo e($errors->has('terminos') ? ' has-error' : ''); ?>">
                              <input type="checkbox" name="terminos" value="1"> He leido y acepto los Términos y Condiciones de Uso
                              <?php if($errors->has('terminos')): ?>
                              <span class="help-block">
                                   <strong> Es necesario aceptar los términos y condiciones.</strong>
                              </span>
                              <?php endif; ?>
                         </div>

                         <div class="form-group">
                              <div class="col-md-12">
                                   <button type="submit" class="btn btn-primary btn-block"  id="ip3">
                                        Regístrate ahora
                                   </button>
                              </div>
                              <div class="col-md-12 m-t-sm ">Si ya tienes una cuenta
                                   <a class=""  href="javascript:history.back()">
                                        inicia sesión
                                   </a><br><br>

                              </div>
                         </div>
                    </form>
               </div>
          </div>
          <span style="font-size: 11px; ">
              Para obtener más información sobre cómo <strong>eduPlace</strong> recopila,<br>
              utiliza, comparte y protege sus datos personales, <br>
              lea el <a href="#"> Aviso de Privacidad</a> de <strong>eduPlace</strong>.
          </span><br><br><br>
     </div>

     <!-- End wrapper-->

     <script src="<?php echo asset('js/app.js'); ?>" type="text/javascript"></script>

     <?php $__env->startSection('scripts'); ?>
     <?php echo $__env->yieldSection(); ?>

</body>
</html>
