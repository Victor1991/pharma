<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Login
Auth::routes();

// Ver formulario Login
Route::get('login/{url?}', 'Auth\LoginController@showLoginForm')->name("login");


//Archivos
Route::get('/brochure/takeda', 'PaginaController@takeda')->name("takeda");


//pagina
Route::get('/', 'PaginaController@index')->name("pagina");
Route::get('/ereii2019', 'PaginaController@pagina_curso')->name("pagina_curso");
Route::get('/speakertraining', 'PaginaController@pagina_curso')->name("pagina_curso");
Route::get('/cursoepoc', 'PaginaController@pagina_curso')->name("pagina_curso");
Route::get('/novonordisk', 'PaginaController@pagina_curso')->name("pagina_curso");
Route::get('/diamundialdelaobesidad', 'PaginaController@pagina_curso')->name("pagina_curso");
Route::get('/programa/{id?}', 'PaginaController@pagina_curso')->name("pagina_curso");
Route::get('/seminario/{url?}', 'PaginaController@cursos')->name("pagina_curso");
Route::get('/{url?}/cursos', 'PaginaController@cursos')->name("pagina_curso");
Route::get('/programa/{url?}/takeda', 'PaginaController@cursos')->name("pagina_curso");
Route::get('/novonordisk/{url?}', 'PaginaController@cursos')->name("pagina_curso");

Route::get('/aviso_privacidad_eduplace', function()
{
    return view('avisos.avisoprivacidad');
});

Route::get('/legales', function()
{
    return view('avisos.legales');
});



// Registro exitos
Route::get('registro_exitoso', 'PaginaController@registor_exito')->name("registro_exitoso");
Route::get('test', 'PaginaController@test_mail')->name("test");
Route::post('validar_correo', 'UsuariosController@validar_correo')->name("validar_correo");
Route::get('insertar', 'PaginaController@insertar')->name("insertar");

Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::get('/registros_curso/{id}', 'PaginaController@informacion_curso')->name('informacion_curso');

Route::get('/casos_clinicos', 'PaginaController@casos_clinicos')->name('casos_clinicos');

Route::get('/inscribir_csv','AlumnosController@inscribir_csv')->name("inscribir_csv");


// Ver formulario de registro
// Route::get('registro', 'AlumnosController@registro')->name('registro');


// Rutas con acceso al logearse
Route::group( ['middleware' => 'auth' ], function(){

     // ------- Cursos -------

     // Incio
     Route::get('/home', 'CursosController@index');

     // Cursos
     // Route::get('/', 'CursosController@index')->name("main");

     // Descripcion del curso
     Route::get('/curso/{id}', 'CursosController@curso')->name("curso");

     // Ver contenido curso
     Route::get('/ver_material/{id}', 'CursosController@ver_material')->name("ver_material");

     // Ver curso anterior
     Route::get('/siguiente/{id}', 'CursosController@siguiente')->name("siguiente");

     // Ver siquiente curso
     Route::get('/anterior/{id}', 'CursosController@anterior')->name("anterior");

     // Finalizar material
     Route::post('/fin_material/{id}', 'CursosController@fin_material')->name("fin_material");


     // Examen
     Route::get('/examen/{id}', 'ExamenController@examen')->name("examen");

     // Pre Examen
     Route::get('/pre_examen/{id}', 'ExamenController@examen')->name("examen");


     // Califiacar examen
     Route::post('/calificar_examen/{id}', 'ExamenController@calificar_examen')->name('calificar_examen');

     // Obtener certificado
     Route::get('/certificado/{id}', 'ExamenController@certificado')->name('certificado');


     Route::get('/mpdf', 'ExamenController@mpdf')->name('mpdf');

     Route::post('/save_especialkey/{id}', 'CursosController@save_especialkey')->name('save_especialkey');

     Route::get('/terminar_sin_examen/{id}', 'CursosController@terminar_curso_sin_examen')->name('terminar_sin_examen');

     //Cerrar sesion
     Route::get('/logout', function(){
        Auth::logout();
        return Redirect::to('/');
     });


     // Editar ver form alumno
     Route::get('/editar_perfil/{id?}', 'AlumnosController@editar')->name("editar_alumno");

     // Editar update DB
     Route::post('/editar_data_perfil/{id}', 'AlumnosController@editar_data_perfil')->name("editar_data_perfil");


     Route::get('/inscribir_siguiente_curso', 'CursosController@inscribir_siguiente_curso')->name("inscribir_siguiente_curso");


     // Reiniciar curso
     Route::get('/reiniciar_curso/{id}', 'CursosController@reiniciar_curso')->name("reiniciar_curso");

     // Route::get('/test/{id}', 'CursosController@finalizar_curso')->name("test");


});
