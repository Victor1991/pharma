<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materiales extends Model
{
     protected $table = 'materiales';
     const CREATED_AT = 'fecha_creacion';
     const UPDATED_AT = 'fecha_actualizacion';


     public function material_tomado()
     {
          return $this->belongsTo('App\Material_tomado', 'id', 'material_id');
     }

     public function preguntas()
     {
          return $this->hasMany(Preguntas::class, 'material_id');
     }

     public function respuestas_correctas($id)
     {
          $preguntas = Preguntas::where('material_id', $id)->get();
          $respod = false;
          $correctas = 0;
          foreach ($preguntas as $key => $pregunta) {
               $respuesta_alumno = $pregunta->respuestas_alumno;
               if ($respuesta_alumno) {
                    $respod = true;
                    if ($respuesta_alumno->respuesta->correcta == 1) {
                         $correctas++;
                    }
               }
          }

          return array('type' => $respod, 'response' => $correctas);
     }

}
