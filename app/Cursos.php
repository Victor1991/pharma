<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Materiales;
use App\Material_tomado;

class Cursos extends Model
{
     protected $table = 'cursos';


     public function materiales()
     {
          return $this->hasMany(Materiales::class, 'curso_id');
     }

     public function pre_examen()
     {
          return $this->hasOne(Examen::class, 'curso_id')->where('examen.tipo', '=', '2');
     }

     public function examen()
     {
          return $this->hasOne(Examen::class, 'curso_id')->where('examen.tipo', '=', '1');
     }

     public function secciones()
     {
          return $this->hasMany(Secciones::class, 'curso_id')->orderBy('orden', 'asc');
     }


}
