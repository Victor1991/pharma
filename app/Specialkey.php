<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialkey extends Model
{
     protected $table = 'specialkey';
     const CREATED_AT = 'fecha_creacion';
     const UPDATED_AT = 'fecha_actualizacion';
}
