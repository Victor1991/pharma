<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuarios extends Authenticatable
{
     use Notifiable;

      protected $table = 'usuarios';
      const CREATED_AT = 'fecha_creacion';
      const UPDATED_AT = 'fecha_actualizacion';


      protected $fillable = [
          'nombre', 'correo', 'password', 'rol_id', 'estatus', 'apellido_paterno', 'apellidos_materno', 'sexo',
      ];

      /**
       * The attributes that should be hidden for arrays.
       *
        @var array
       */
      protected $hidden = [
          'password', 'token',
      ];

     public function alumno() {
          return $this->hasOne(Alumnos::class, 'usuario_id');
     }


}
