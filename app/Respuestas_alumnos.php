<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuestas_alumnos extends Model
{
     protected $table = 'respuestas_alumno';
     const CREATED_AT = 'fecha_creacion';
     const UPDATED_AT = 'fecha_actualizacion';

     public function respuesta()
     {
          return $this->hasOne( Respuestas::class, 'id', 'respuesta_id' );

     }
}
