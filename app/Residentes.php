<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Residentes extends Model{
     const CREATED_AT = NULL;
     const UPDATED_AT = NULL;

    protected $table = 'catalogo_residentes';
}
