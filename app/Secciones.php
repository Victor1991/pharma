<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secciones extends Model
{
     protected $table = 'secciones';
     const CREATED_AT = 'fecha_creacion';
     const UPDATED_AT = 'fecha_actualizacion';

     public function materiales()
     {
          return $this->hasMany(Materiales::class, 'seccion_id');

     }
}
