<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recurso_relacionados_curso extends Model
{
     const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    protected $table = 'recurso_relacionados_curso';
}
