<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas extends Model
{
     public function respuestas()
     {
          return $this->hasMany( Respuestas::class, 'pregunta_id', 'id' );
     }

     public function respuestas_alumno()
     {
          return $this->hasOne( Respuestas_alumnos::class, 'pregunta_id', 'id' )->where('intento_id', '=', session('intento_id'));

     }

}
