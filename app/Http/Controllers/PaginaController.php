<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Catalogos;
use App\Cursos;
use App\Alumnos_cursos;
use App\Materiales;
use App\Secciones;
use App\Intentos;
use App\Material_tomado;
use App\Alumnos;
use App\Examen;
use App\Preguntas;
use App\Respuestas_alumnos;
use App\Respuestas;
use App\Usuarios;
use App\Estado;
use App\Pais;

use Illuminate\Support\Facades\DB;


use Mail;

class PaginaController extends Controller{

     public function index(){
          return view('pagina/pagina', []);
     }

     public function takeda()
     {
          return view('pdf/takeda', []);

     }

     public function cursos($url)
     {
          $curso_id = Cursos::select('id')->where('url', $url)->first();
          $curso = $url;
          return view('pagina/pagina_url', compact('curso_id','curso'));
     }

     public function pagina_curso(){
          $url = $_SERVER['REQUEST_URI'];
          $url = str_replace("programa", "", $url);
          $url = str_replace("/", "", $url);
          $data['curso_id'] = Cursos::select('id')->where('url', $url)->first();
          $data['curso'] = $url;
          return view('pagina/pagina_url', $data);
     }

     public function registor_exito(){
          return view('pagina/registro_exito', []);
     }

     public function exito_registro(){
          return view('pagina/exito_registro', []);
     }


     public function test_mail(){
          Mail::send('mails.demo', [] , function($msj){
               $msj->subject('correo');
               $msj->to('fu-sio@hotmail.com');
          });
     }

     public function informacion_curso($id){
          $curso = Cursos::find($id);



          // dd($secciones_respuestas);
          //
          // die;

          $materiales = $curso->materiales->count();

          $alumnos = DB::table('alumnos_cursos')->Join('usuarios', 'alumnos_cursos.usuario_id', '=', 'usuarios.id')
          ->where('alumnos_cursos.curso_id',$curso['id'])->get();


          $data = array('curso' => $curso);
          $edades = array();
          $sexo = array();
          $preguntas_pre = array();
          $preguntas_post = array();

          if ( $curso->pre_examen) {
               $preguntas_pre = Preguntas::where('examen_id',  $curso->pre_examen->id)->get();
          }

          if ($curso->examen) {
               $preguntas_post = Preguntas::where('examen_id',$curso->examen->id)->get();
          }


          $data['materiales'] = array();
          $secciones = $curso->secciones;
          $j = 0;
          $d = 0;




          foreach ($secciones as $key_secciones => $seccion) {
               $materiales_secciones = Secciones::find($seccion['id'])->materiales;

               foreach ($materiales_secciones as $key_materiales => $material) {

                    $data['materiales'][$j]['material'] = $material;

                    if ($material['tipo_archivo'] == "specialkey") {
                         $preguntas =  Preguntas::where('specialkey', $material['specialkey_id'])->get();
                    }else {
                         $preguntas = Materiales::find($material['id'])->preguntas;
                    }
                    $d +=$preguntas->count();
                    $data['materiales'][$j]['material']['n_preguntas'] = $preguntas->count();
                    $data['materiales'][$j]['pregunta'] = $preguntas;
                    $j++;
               }
          }



               // dump($preguntas);
               //
               // die;
          $data['usuarios_sin_empezar'] = array();
          $sr =  0;
          foreach ($alumnos as $key => $alumno) {
               //sexo
               if(array_key_exists($alumno->sexo,$sexo)) {
                    $sexo[$alumno->sexo] += 1;
               }else{
                    $sexo[$alumno->sexo] = 1;
               }
               // Edad
               $usu = Alumnos::where('usuario_id', $alumno->usuario_id)->first();


               if ($usu) {

                    $alumno->edad = $usu->edad;
                    $alumno->ciudad = $usu->ciudad;
                    $alumno->cedula = $usu->cedula;
                    if(array_key_exists($usu->edad,$edades)) {
                         $edades[$usu->edad] += 1;
                    }else{
                         $edades[$usu->edad] = 1;
                    }

                    if ($usu->pais_id == 0) {
                         $alumno->estado = 'sin registro';
                    }else {
                         $pais = Pais::where('id', $usu->pais_id)->first();
                         $alumno->pais = $pais->nombre;
                    }

                    if ($usu->estado_id == 0) {
                         $alumno->estado = 'sin registro';
                    }else {
                         $estado = Estado::where('id', $usu->estado_id)->first();
                         $alumno->estado = $estado->nombre;

                    }

               }else{
                    $alumno->edad = 'sin_registro';
                    $sr += 1;
               }

               $intentos = Intentos::where('curso_id', $id)->where('usuario_id', $alumno->id)->get();

               if (count($intentos) == 0 ) {
                    $data['usuarios_sin_empezar'][] = $alumno;
               }

               foreach ($intentos as $key2 => $intento) {
                    $data['alumnos'][$key]['intentos'][$key2]['calificacion'] =  $intento->calificacion;
                    $data['alumnos'][$key]['intentos'][$key2]['pre_respuetas'] =  array();
                    if (count($preguntas_pre) > 0) {
                         foreach ($preguntas_pre as $key3 => $pregunta) {

                              $respuestas = DB::table('respuestas_alumno')->where('pregunta_id',$pregunta->id)->where('intento_id',$intento->id)->first();
                              if ($respuestas) {
                                   $respu = Respuestas::where('id', $respuestas->respuesta_id)->first();
                              }else{
                                   $respu = array();
                              }

                              $data['alumnos'][$key]['intentos'][$key2]['pre_respuetas'][] = $respu;
                         }

                    }

                    foreach ($data['materiales'] as $key_m => $mat) {
                         foreach ($mat['pregunta'] as $key_p => $pre) {

                              $respuestas = DB::table('respuestas_alumno')->where('pregunta_id',$pre->id)->where('intento_id',$intento->id)->first();
                              if ($respuestas) {
                                   $respu = Respuestas::where('id', $respuestas->respuesta_id)->first();
                              }else{
                                   $respu = array();
                              }

                              $data['alumnos'][$key]['intentos'][$key2]['material'][] = $respu;
                         }
                    }


                    $data['alumnos'][$key]['intentos'][$key2]['post_respuetas'] =  array();
                    if (count($preguntas_post) > 0) {
                         foreach ($preguntas_post as $key3 => $pregunta) {
                              $respuestas = DB::table('respuestas_alumno')->where('pregunta_id',$pregunta->id)->where('intento_id',$intento->id)->first();
                              if ($respuestas) {
                                   $respu = Respuestas::where('id', $respuestas->respuesta_id)->first();
                              }else{
                                   $respu = array();
                              }

                              $data['alumnos'][$key]['intentos'][$key2]['post_respuetas'][] = $respu;
                         }
                    }

                    $data['alumnos'][$key]['alumno'] = $alumno;
                    $data['alumnos'][$key]['alumno']->instituto = @$usu->instituto->nombre;
                    $data['alumnos'][$key]['alumno']->especialidad = @$usu->especialidad->nombre;
                    $data['alumnos'][$key]['intentos'][$key2]['total_materiales'] = $materiales;
                    $data['alumnos'][$key]['intentos'][$key2]['materiales_finalizados'] = Material_tomado::where('usuario_id' ,$alumno->id)->where('intento_id', $intento->id)->where('finalizado', '1')->count();
               }
          }


          asort($edades);
          $data['edades'] = $edades;
          $data['edades']['sin_registro'] = $sr;
          asort($sexo);
          $data['sexo'] = $sexo;



          $data['preguntas_pre'] = $preguntas_pre;
          $data['preguntas_post'] = $preguntas_post;


          return view('reporte.index', $data);
     }

     public function casos_clinicos(){
          $materiales = Materiales::whereNotNull('specialkey_id')->get();
          $cont = 0;
          foreach ($materiales as $key_mat => $material) {
               $intentos =  Intentos::where('curso_id', $material['curso_id'])->where('finalizado', 1)->get();
               $preguntas = Preguntas::where('specialkey', $material['specialkey_id'])->get();
               foreach ($preguntas as $key_pregunta => $preguna) {
                    $respuestas = Respuestas::where('pregunta_id', $preguna['id'])->get();
                    foreach ($intentos as $key_intento => $intento) {


                         if ($respuestas) {
                              $respuest  = $respuestas->toArray();
                              if ($cont % 3 == 0) {
                                   shuffle($respuest);
                                   $repusta = $respuest[0]['id'];
                              }else{
                                   $repusta = array_search('1', array_column($respuest, 'correcta' , 'id'));

                              }
                         }


                         $existe_repusta = Respuestas_alumnos::where('intento_id', $intento['id'])->where('pregunta_id', $preguna['id'])->where('usuario_id', $intento['usuario_id'])->first();

                         if (!$existe_repusta) {
                              $resp = new Respuestas_alumnos();
                              $resp->pregunta_id = $preguna['id'];
                              $resp->respuesta_id = $repusta;
                              $resp->usuario_id = $intento['usuario_id'];
                              $resp->intento_id = $intento['id'];
                              $resp->save();
                              echo "<br>---- Listo ------<br>";

                         }
                         $cont++;
                    }
               }

          }
     }
}
