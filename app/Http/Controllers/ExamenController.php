<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Examen;
use App\Preguntas;
use App\Respuestas;
use App\Respuestas_alumnos;
use App\Respuestas_preguntas;
use App\Intentos;
use App\Cursos;
use App\Materiales;
use App\Material_tomado;


use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFundation\Response;

use Codedge\Fpdf\Fpdf\Fpdf;

class ExamenController extends Controller
{

     private $fpdf;

    public function examen($examen_id)
    {
         $data['examen'] = Examen::where('id', $examen_id)->first()->toArray();
         $data['examen']['tiempo'] = $this->convert_time_seconds( $data['examen']['tiempo']) ?  $this->convert_time_seconds( $data['examen']['tiempo']) : false;
         $preguntas = Preguntas::where('examen_id', $data['examen']['id'])->get()->toArray();
         foreach ($preguntas as $key => $pregunta) {
              $data['preguntas'][$key]['pregunta'] = $pregunta;
              $data['preguntas'][$key]['respuesta'] =  Respuestas::where('pregunta_id',$pregunta['id'])->orderBy('opcion', 'asc')->get()->toArray();
         }
         return view('examen.form', $data);
    }

    public function convert_time_seconds($time)
    {
         $parsed = date_parse($time);
         return $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
    }

    public function get_respuesta_alumno($pregunta_id, $intento_id){
         return Respuestas_alumnos::where('pregunta_id', $pregunta_id)->where('intento_id', $intento_id)->first();
    }

    public function ultimo_intenoto_curo($curso_id){
         $usuario_id = \Auth::user()->id;
         $intentos = Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->where('finalizado', '1')->count();
         $en_curso = Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->whereNull('fin')->count();


         if ($en_curso >= 1) {
              $intento =  Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->whereNull('fin')->orderBy('id', 'desc')->first();
               return $intento->id;
         }elseif ($intentos >= 1) {
                $intento =  Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->where('finalizado', '1')->orderBy('id', 'desc')->first();
               return $intento->id;
         }else{
              $intento = new Intentos;
              $intento->usuario_id = $usuario_id;
              $intento->examen_id = 0;
              $intento->inicio = date('Y-m-d H:i:s');
              $intento->num_intento = '1';
              $intento->curso_id = $curso_id;
              $intento->save();
              return $intento->id;
         }
    }

     public function calificar_examen($examen_id, Request $request)
     {

          $usuario_id = \Auth::user()->id;
          $fecha_inicio = $request['f_inicio'];
          $examen = Examen::where('id', $examen_id)->first()->toArray();
          $intento = $this->ultimo_intenoto_curo($examen['curso_id']);


          if (isset($request['pregunta'])) {
               if (count($request['pregunta']) > 0) {
                    foreach ($request['pregunta'] as $pregunta_id => $respuesta) {
                         $pregunta = $this->get_respuesta_alumno($pregunta_id, session('intento_id'));
                         if ($pregunta) {
                              Respuestas_alumnos::where('pregunta_id', $pregunta)
                                                       ->where('intento_id', $intento)
                                                       ->update(['respuesta_id' => $respuesta]);
                         }else{
                              $resp = new Respuestas_alumnos();
                              $resp->pregunta_id = $pregunta_id;
                              $resp->respuesta_id = $respuesta;
                              $resp->usuario_id = $usuario_id;
                              $resp->intento_id = $intento;
                              $resp->save();
                         }
                    }
               }
          }

          // Examen PRE


          if ($examen['tipo'] == 2) {
               return redirect('/curso/'.$examen['curso_id']);
          }

          $aprobado = $this->obtener_calificacion($intento, $examen_id);

          if($aprobado){
               if ($examen['porcentaje_aprobar'] == 0) {
                    return redirect('home')->with('message', 'por haber concluido el curso.');
               }else{
                    return redirect('home')->with('message', 'por haber concluido el curso.');
               }
          }else{
               return redirect('home')->with('error', 'Intentalo de nuevo, has reprobado');
          }
     }

     /*
          1. RESPUESTAS PRE EXAMEN
          2. RESPUESTAS CASO CLINICO
          3. RESPUESTAS MODULOS
          4. RESPUESTAS POST EXAMEN
     */
     public function respuestas_type($id, $type){
          switch ($type) {
               case '1':
                    return DB::table('preguntas')
                         ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                         ->where('respuestas_preguntas.correcta', '1')
                         ->where('preguntas.examen_id', $id)->get()->toArray();
               break;
               case '2':
                    return DB::table('preguntas')
                              ->leftJoin('materiales', 'materiales.specialkey_id', '=', 'preguntas.specialkey')
                              ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                              ->where('respuestas_preguntas.correcta', '1')
                              ->where('materiales.curso_id', $id)
                              ->get()->toArray();
               break;
               case '3':
                    return DB::table('preguntas')
                         ->leftJoin('materiales', 'materiales.id', '=', 'preguntas.material_id')
                         ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                         ->where('respuestas_preguntas.correcta', '1')
                         ->where('materiales.curso_id', $id)
                         ->get()->toArray();

               break;
               case '4':
                    return DB::table('preguntas')
                         ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                         ->where('respuestas_preguntas.correcta', '1')
                         ->where('preguntas.examen_id', $examen_id)->get()->toArray();
               break;
          }
     }


     public function obtener_calificacion($intento_id, $examen_id)
     {

          $info_examen = Examen::where('id', $examen_id)->first()->toArray();
          $info_curso = Cursos::find($info_examen['curso_id'])->toArray();


          // Obtener preguntas con respuesta correctas
          $respuestas =  DB::table('preguntas')->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')->where('respuestas_preguntas.correcta', '1')->where('preguntas.examen_id', $examen_id)->get()->toArray();

          // Obtener respuestas del usuario
          $respuestas_usuario = Respuestas_alumnos::where('intento_id', $intento_id)->get()->toArray();

          // contador respuestas correctas
          $conteo = 0;
          $conteo_a_calificar = 0;
          $sum_total = 0;
          // contador de preguntas
          $conteo_pregntas = count($respuestas);


          // respuestas validar_pre_examen L
          if ($info_curso['calificar_pre_examen']) {
               $conteo_a_calificar++;
               $new_examen = Examen::where('curso_id', '=', $info_curso['id'])->where('tipo', '=',  '2')->first()->toArray();
               $respuestas = $this->respuestas_type($new_examen['id'], '1');
               $respuestas_correctas  = array_column($respuestas, 'id');
               $respuestas_correctas = Respuestas_alumnos::whereIn('respuesta_id', $respuestas_correctas)->where('intento_id', $intento_id)->get()->toArray();
               $count_preguntas = count($respuestas);
               $count_respuestas = count($respuestas_correctas);
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;
          }


          // respuestas caso clinico
          if ($info_curso['calificar_caso_clinico']) {
               $conteo_a_calificar++;
               $respuestas = $this->respuestas_type($info_curso['id'], '2');
               $respuestas_cor  = array_column($respuestas, 'id');
               $respuestas_correctas = Respuestas_alumnos::whereIn('respuesta_id', $respuestas_cor)->where('intento_id', $intento_id)->get()->toArray();
               $count_preguntas = count($respuestas);
               $count_respuestas = count($respuestas_correctas);
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;
          }

          // respuestas post modulo L
          if ($info_curso['calificar_preguntas_modulo']) {
               $conteo_a_calificar++;
               $respuestas = $this->respuestas_type($info_curso['id'], '3');
               $respuestas_cor = array_column($respuestas, 'id');
               $respuestas_correctas = Respuestas_alumnos::whereIn('respuesta_id', $respuestas_cor)->where('intento_id', $intento_id)->get()->toArray();
               $count_preguntas = count($respuestas_cor);
               $count_respuestas = count($respuestas_correctas);
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;

          }

          // respuesta port_examen
          if ($info_curso['calificar_post_examen']) {
               $conteo_a_calificar++;
               $new_examen = Examen::where('curso_id', '=', $info_curso['id'])->where('tipo', '=',  '1')->first()->toArray();
               $respuestas = $this->respuestas_type($new_examen['id'], '1');
               $respuestas_correctas  = array_column($respuestas, 'id');
               $respuestas_correctas = Respuestas_alumnos::whereIn('respuesta_id', $respuestas_correctas)->where('intento_id', $intento_id)->get()->toArray();
               $count_preguntas = count($respuestas);
               $count_respuestas = count($respuestas_correctas);
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;

          }

          // obtener informacion de examen
          $examen =  Examen::where('id', $examen_id)->first()->toArray();

          // porcentaje para aprobar el examen
          $aprobar = $examen['porcentaje_aprobar'];



          if ($sum_total == 0 && $conteo_a_calificar == 0  ) {
               $total =  0;
          }else{
               // promedio obtenido
               $total =  number_format(($sum_total  / $conteo_a_calificar), 2);
          }


          $intento['aprobado'] = 0;
          $intento['calificacion'] = $total;
          if ($aprobar <= $total) {
               $intento['aprobado'] = 1;
          }
          $intento['finalizado'] = 1;
          $intento['fin'] = date('Y-m-d H:m:s');

          Intentos::where('id', $intento_id)->update($intento);
          return $intento['aprobado'];
     }


     public function certificado($curso_id)
     {


          $usuario = \Auth::user();
          $curso = Cursos::find($curso_id);
          // $usuario = DB::table('alumnos')->leftJoin('usuarios', 'alumnos.usuario_id', '=', 'usuarios.id')->where('usuarios.id', $usuario->id )->first();

          $aprobado =  $respuestas =  DB::table('intentos')->where('curso_id', $curso_id)->where('aprobado', '1')->where('usuario_id', $usuario)->first();

          //
          // if (is_null(aprobado)) {
          //      return redirect('/')->with('error', 'Certificado no encontrado');
          // }


          // return view('certificado.certificado', $data);



          $this->fpdf = new Fpdf;
          $this->fpdf->AddPage('O');
          $this->fpdf->Image('img/'.$curso->img_certificado, 0, 0, 300, 210);

          $this->fpdf->SetFont('Arial','',24);
          $this->fpdf->SetXY(100, 50);
          $this->fpdf->Write(0, utf8_decode($usuario->nombre. ' ' .$usuario->apellido_paterno. ' ' . $usuario->apellidos_materno));

          $this->fpdf->Output('D', 'Certificado '.utf8_decode($curso->nombre).'.pdf');

          exit;

     }

     public function mpdf($accion='ver',$tipo='digital'){

     }

}
