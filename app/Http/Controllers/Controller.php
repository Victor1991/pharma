<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected static $js_files  = [];
    protected static $css_files = [];

    public static function boot()
    {
          view()->share('js_files', static::$js_files);
          view()->share('css_files', static::$css_files);
          parent::boot();
     }
}
