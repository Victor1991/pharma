<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Alumnos_cursos;

class LoginController extends Controller
{
     /*
     |--------------------------------------------------------------------------
     | Login Controller
     |--------------------------------------------------------------------------
     |
     | This controller handles authenticating users for the application and
     | redirecting them to your home screen. The controller uses a trait
     | to conveniently provide its functionality to your applications.
     |
     */

     use AuthenticatesUsers;

     /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/home';

     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
          // $this->middleware('guest', ['except' => 'logout']);

          $this->middleware('guest')->except('logout');

     }

     protected function authenticated(Request $request, $user)
     {
          if ($request->curso_id != "") {
               $curso = Alumnos_cursos::where('usuario_id', $user->id)->where('curso_id', $request->curso_id)->first();

               if ($request->curso_id == 12 || $request->curso_id == 13 || $request->curso_id == 14 || $request->curso_id == 15 ) {
                    for($i=12; $i <=15 ; $i++) {
                         if (is_null(Alumnos_cursos::where('usuario_id', $user->id)->where('curso_id', $i)->first())) {
                              $user_id = $user->id;
                              $cur_alumn = new Alumnos_cursos();
                              $cur_alumn->usuario_id = $user_id;
                              $cur_alumn->curso_id = $i;
                              $cur_alumn->save();
                         }

                    }
               }elseif ($request->curso_id == 16 || $request->curso_id == 17 || $request->curso_id == 18) {
                    for($i=16; $i <=18 ; $i++) {
                         if (is_null(Alumnos_cursos::where('usuario_id', $user->id)->where('curso_id', $i)->first())) {
                              $user_id = $user->id;
                              $cur_alumn = new Alumnos_cursos();
                              $cur_alumn->usuario_id = $user_id;
                              $cur_alumn->curso_id = $i;
                              $cur_alumn->save();
                         }
                    }
               }elseif ($request->curso_id == 8) {
                    $cuss = array('6','8','10','19', '27', '30');
                    foreach ($cuss as $key => $i) {
                         if (is_null(Alumnos_cursos::where('usuario_id', $user->id)->where('curso_id', $i)->first())) {
                              $user_id = $user->id;
                              $cur_alumn = new Alumnos_cursos();
                              $cur_alumn->usuario_id = $user_id;
                              $cur_alumn->curso_id = $i;
                              $cur_alumn->save();
                         }
                    }
               }elseif ($request->curso_id == 21) {
                    $cuss = array('21','24','26');
                    foreach ($cuss as $key => $i) {
                         if (is_null(Alumnos_cursos::where('usuario_id', $user->id)->where('curso_id', $i)->first())) {
                              $user_id = $user->id;
                              $cur_alumn = new Alumnos_cursos();
                              $cur_alumn->usuario_id = $user_id;
                              $cur_alumn->curso_id = $i;
                              $cur_alumn->save();
                         }
                    }
               }else {
                    if (is_null(Alumnos_cursos::where('usuario_id', $user->id)->where('curso_id', $request->curso_id)->first())) {
                         $user_id = $user->id;
                         $cur_alumn = new Alumnos_cursos();
                         $cur_alumn->usuario_id = $user->id;
                         $cur_alumn->curso_id = $request->curso_id;
                         $cur_alumn->save();
                    }
               }


          }
     }
}
