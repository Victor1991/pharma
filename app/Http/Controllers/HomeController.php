<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Alumnos_cursos;
use App\Clientes;
use App\Cursos;
use App\Materiales;
use App\Material_tomado;
use App\Materiales;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \Auth::user()->id;
        $cursos = Alumnos_cursos::where('usuario_id', $id)->get()->toArray();
        $data['cursos'] = array();
        foreach ($cursos as $key => $curso) {
            $curso = Cursos::find($curso['curso_id'])->toArray();
            $data['cursos'][$key] = $curso;
            $data['cursos'][$key]['porcentaje'] = $this->porcentaje_curso($curso['id']);
            $data['cursos'][$key]['cliente'] = Clientes::find($curso['cliente_id'])->toArray();
        }
        return view('home', $data);
    }

     public function porcentaje_curso($curso_id)
     {
          $material = Materiales::select('id')->where('curso_id', $curso_id)->get()->toArray();
          $ids = array_column($material, 'id');
          $c_terminados = Material_tomado::select('id')->whereIn('material_id', $ids)->get()->toArray();
          $finalizados = count($c_terminados);
          $total =  count($material);
          $porcentaje  = $finalizados * 100 / (count($material)+1);
          return array( round($porcentaje, 2), $total , $finalizados);
     }




}
