<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use App\Especialidades;
use App\Alumnos;
use App\Pais;
use App\Residentes;
use App\Instituciones;
use App\Alumnos_cursos;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFundation\Response;



class AlumnosController extends Controller
{

     public function editar( $id = null)
     {
          $data['id'] = isset($id) ? $id : \Auth::user()->id ;
          $data['especialidades'] = Especialidades::orderByDesc('id')->get()->toArray();
          $data['pais'] = Pais::orderBy('nombre')->get()->toArray();
          $data['residentes'] = Residentes::orderBy('nombre')->get()->toArray();
          $data['instituciones'] = Instituciones::orderBy('nombre')->get()->toArray();
          $data['usuario'] = DB::table('alumnos')->leftJoin('usuarios', 'alumnos.usuario_id', '=', 'usuarios.id')->where('usuarios.id', $data['id'] )->first();

          return view('alumnos/editar', $data);
     }

     public function editar_data_perfil($id, Request $request)
     {

          $Validar = $this->validate($request, [
              'nombre' => 'required|max:255',
              'apellido_paterno' => 'required|max:255',
              'sexo' => 'required|max:255',
              'edad' => 'required|max:255',
              'pais' => 'required|max:255',
              'ciudad' => 'required|max:255',
              'codigo_postal' => 'required|max:255',
              'cedula_profecional' => 'required|max:255',
              'especialidad' => 'required|max:255',
              'institucion' => 'required|max:255',
              'correo' => 'unique:usuarios,correo,'.$id,
          ]);


          if ($request['password']) {
               $Validar = $this->validate($request, [ 'password' => 'required|min:6|confirmed']);
          }




          // Modifciar Usuario
          $usuarios = array();
          if (isset($request->nombre)){ $usuarios['nombre'] = $request->nombre; }
          if (isset($request->apellido_paterno)){ $usuarios['apellido_paterno'] = $request->apellido_paterno;  }
          if (isset($request->apellidos_materno)){ $usuarios['apellidos_materno'] = $request->apellidos_materno;  }
          if (isset($request->sexo)){ $usuarios['sexo'] = $request->sexo;  }
          if (isset($request->correo)) { $usuarios['correo'] = $request->correo; }
          if (isset($request->password)){
               if ($request->password != '') {
                    $usuarios['password'] = bcrypt($request->password);
               }
          }

          if (count($usuarios) > 0) {
               $affectedRows = Usuarios::where('id', $id)->update($usuarios);
          }

          //Modifciar Alumno
          $alumno = array();
          if (isset($request->especialidad)){ $alumno['especialidad_id'] = $request->especialidad; }
          if (isset($request->telefono)){ $alumno['telefono'] = $request->telefono; }
          if (isset($request->celular)){ $alumno['celular'] = $request->celular; }
          if (isset($request->cedula_profecional)){ $alumno['cedula'] = $request->cedula_profecional; }
          if (isset($request->edad)){ $alumno['edad'] = $request->edad; }
          if (isset($request->pais)){ $alumno['pais_id'] = $request->pais; }
          if (isset($request->ciudad)){ $alumno['ciudad'] = $request->ciudad; }
          if (isset($request->codigo_postal)){ $alumno['cp'] = $request->codigo_postal; }
          if (isset($request->institucion)){ $alumno['institucion_id'] = $request->institucion; }
          if (count($usuarios) > 0) {
               $affectedRows = Alumnos::where('usuario_id', $id)->update($alumno);
          }

          return redirect('editar_perfil')->with('message', 'Usuario modificado correctamente');

     }


     public function registro(Request $request)
     {
          $data['curso_id'] = $request->curso_id;
          $data['correo'] = $request->correo;
          $data['especialidades'] = Especialidades::orderByDesc('id')->get()->toArray();
          return view('auth.register', $data );
          // return view('alumnos.editar', $data );
     }

     public function inscribir_csv()
     {
          $filename = 'Registrados takeda.csv';
          $file = fopen($filename, "r");
          while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
          {
               $cursos = array(8,9,10);
               $alumno_id = $getData[0];
               foreach ($cursos as  $curso) {

                    $inscrito = Alumnos_cursos::where('usuario_id',$alumno_id)->where('curso_id', $curso)->first();
                    if ($inscrito) {
                         dump('<br>----------inscrito----------<br>');
                    }else{
                         dump('<br>----------por inscrito----------<br>');
                         $inscrito = new Alumnos_cursos;
                         $inscrito->usuario_id = $alumno_id;
                          $inscrito->curso_id = $curso;
                          $inscrito->save();
                         dump($inscrito);
                    }

               }
          }

     }


}
