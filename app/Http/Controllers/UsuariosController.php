<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;


class UsuariosController extends Controller
{

     public function validar_correo(Request $request)
     {

          $Validar = $this->validate($request, [
              'correo' => 'required|max:255'
          ]);

          $mensaje =  array( 'type' => 'error' , 'mensaje' => 'ocurrió un error' );
          $curso = Usuarios::where('correo', $request->correo)->first();

          if ($curso) {
               $curso = $curso->toArray();
               if ($curso['estatus'] == 1) {
                    if ($curso['eliminado'] == 0) {
                         $mensaje['type'] = 'exito';
                         $mensaje['mensaje'] = $request->correo;
                    }else {
                         $mensaje['mensaje'] = 'El usuario esta dado de baja';
                    }
               }else{
                    $mensaje['mensaje'] = 'El usuario esta bloqueado';
               }
          }else {
               $mensaje['type'] = 'info';
               $mensaje['correo'] = $request->correo;
               $mensaje['mensaje'] = 'No existe el usuario';
          }
          return response()->json($mensaje);
     }

}
