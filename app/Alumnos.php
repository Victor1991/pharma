<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnos extends Model
{
     protected $table = 'alumnos';
     const CREATED_AT = null;
     const UPDATED_AT = null;

     public function usuario()
     {
          return $this->belongsTo('App\Usuarios', 'usuario_id', 'id');
     }

     public function instituto()
     {
          return $this->hasOne(Instituciones::class, 'id', 'institucion_id');
     }

     public function especialidad()
     {
          return $this->hasOne(Especialidades::class, 'id', 'especialidad_id');
     }

}
