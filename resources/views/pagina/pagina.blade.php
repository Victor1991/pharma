<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->

    <head>

        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>eduPlace</title>
        <meta name="description" content="La comunidad médica informada">
        <meta name="author" content="eduPlace">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" <!-- Favicons -->
              <link rel="shortcut icon" href="img/ico.ico">

        <!-- FONTS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,900'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Poppins:100,300,400,400italic,700'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Nunito:100,300,400,400italic,700'>

        <!-- CSS -->
        <link rel='stylesheet' href="{!! asset('pagina/css/global.css') !!}">
        <link rel='stylesheet' href="{!! asset('pagina/css/structure2.css') !!}">
        <link rel='stylesheet' href="{!! asset('pagina/css/sitter2.css') !!}">
        <link rel='stylesheet' href="{!! asset('pagina/css/custom2.css') !!}">

        <!-- Revolution Slider -->
        <link rel="stylesheet" href="plugins/rs-plugin-5.3.1/css/settings.css">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155648111-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-155648111-1');
        </script>

    </head>

    <body class="home page-template-default page template-slider  color-custom style-simple button-default layout-full-width one-page if-overlay no-shadows header-transparent sticky-header sticky-tb-color ab-hide subheader-both-center menu-link-color menuo-right menuo-no-borders mobile-tb-center mobile-mini-mr-ll tablet-sticky mobile-header-mini mobile-sticky">
        <div id="Wrapper">
            <div id="Header_wrapper">
                <header id="Header">
                    <div id="Top_bar" style="background-color: #fff;">
                        <div class="container">
                            <div class="column one">
                                <div class="top_bar_left clearfix">
                                    <div class="logo">
                                        <a id="logo" href="#" title="eduPlace" data-height="60" data-padding="15">
                                            <img class="logo-main scale-with-grid" src="{!! asset('pagina/img/logo.png') !!}" data-retina="{!! asset('pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina>
                                            <img class="logo-sticky scale-with-grid" src="{!! asset('pagina/img/logo.png') !!}" data-retina="{!! asset('pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina>
                                            <img class="logo-mobile scale-with-grid" src="{!! asset('pag     ina/img/logo.png') !!}" data-retina="{!! asset('pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina>
                                            <img class="logo-mobile-sticky scale-with-grid" src="{!! asset('pagina/img/logo.png') !!}" data-retina="{!! asset('pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina></a>
                                    </div>
                                    <div class="menu_wrapper">
                                        <nav id="menu">
                                            <ul id="menu-menu" class="menu menu-main">
                                                <li>
                                                    <a href="{{ url('/login') }}"><span>INICIAR SESIÓN</span></a>
                                                </li>
                                                <li>
                                                    <a href="{{ url('/register') }}"><span>REGÍSTRATE AHORA</span></a>
                                                </li>
                                                <!--
                                                <li>
                                                     <a href="#"><span>AYUDA</span></a>
                                                </li>
                                                -->

                                            </ul>
                                        </nav><a class="responsive-menu-toggle" href="#"><i class="icon-menu-fine"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="mfn-main-slider" id="mfn-rev-slider">
                        <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                            <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8.3">
                                <ul>
                                    <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide"
                                        data-param1 data-param2 data-param3 data-param4 data-param5 data-param6 data-param7 data-param8 data-param9 data-param10 data-description>
                                        <img src="{!! asset('pagina/img/slider1.png') !!}" title="sitter2-home-slider-bg2" width="1920" height="937" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>






                                        <div class="tp-caption " id="slide-1-layer-1" data-x="center" data-hoffset="" data-y="120" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:200,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; line-height: 28px;  transition: none 0s ease 0s; text-align: center; border-width: 0px; margin: 0px; padding: 0px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: translate3d(0px, 0px, 0px); transform-origin: 50% 50% 0px;">
                                            <img src="{!! asset('pagina/img/logo2.png') !!}">

                                        </div>
                                        <div class="tp-caption " id="slide-3-layer-5" data-x="center" data-y="310" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 28px; line-height: 28px; font-weight: 200; color: #fff; letter-spacing: 0px;">


                                            Es una comunidad médica informada,
                                            <br> te invitamos a formar parte de ella.<br><br>

                                        </div>





                                        <div class="tp-caption " id="slide-1-layer-1" data-x="center" data-hoffset="" data-y="440" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:200,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; line-height: 28px;  transition: none 0s ease 0s; text-align: center; border-width: 0px; margin: 0px; padding: 0px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: translate3d(0px, 0px, 0px); transform-origin: 50% 50% 0px;">

                                            <a href="{{ url('/login') }}"    style=" margin-top: 30px; white-space: nowrap; font-size: 20px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Arial;  background-color: #0095eb;    border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; padding: 10px 30px 10px 30px">
                                                Iniciar sesión
                                            </a><br>



                                        </div>


                                        <div class="tp-caption " id="slide-3-layer-5" data-x="center" data-y="590" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                             data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 28px; line-height: 28px; font-weight: 200; color: #fff; letter-spacing: 0px;">

                                            <center>

                                                Si aún no tienes cuenta,<br><br>


                                                <a href="{{ url('/register') }}"    style=" margin-top: 30px; white-space: nowrap; font-size: 20px; line-height: 40px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Arial;  background-color: #0095eb;    border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; padding: 10px 30px 10px 30px; ">
                                                    Regístrate aquí
                                                </a>
                                            </center>


                                        </div>






                                        </div>





                                    </li>

                                </ul>
                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div id="Content">
                <div class="content_wrapper clearfix">
                    <div class="sections_group">
                        <div class="entry-content">






                            <div class="section mcb-section mcb-section-9w4om4i9s" id="work" style="padding-top:40px;padding-bottom:25px;background-color:#faf9fe;">
                                <div class="section_wrapper mcb-section-inner">
                                    <div class="wrap mcb-wrap mcb-wrap-8545a4r3b one  valign-top clearfix">
                                        <div class="mcb-wrap-inner">

                                            <div class="column mcb-column mcb-item-aahg8szpi one column_column column-margin-10px">
                                                <div class="column_attr clearfix align_center" style>
                                                    <h2 style="color: #556c98;">CONTENIDO EDUCATIVO</h2>
                                                    <h3 style="color:#556c98;">en formatos innovadores y flexibles.</h3>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="wrap mcb-wrap mcb-wrap-816gse6yc one-third  valign-top clearfix" style="padding:0 2%">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-1icjlcvbz one column_icon_box">
                                                <div class="icon_box icon_position_top no_border">

                                                    <center> <img src="{!! asset('pagina/img/ico0.png') !!}"></center>

                                                    <div class="desc_wrapper">
                                                        <h4 class="title">ENTREVISTAS</h4>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap mcb-wrap mcb-wrap-87gbte1zy one-third  valign-top clearfix" style="padding:0 2%">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-n1x859fk1 one column_icon_box">
                                                <div class="icon_box icon_position_top no_border">
                                                    <center><img src="{!! asset('pagina/img/ico1.png') !!}"></center>
                                                    <div class="desc_wrapper">
                                                        <h4 class="title">CÁPSULAS</h4>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap mcb-wrap mcb-wrap-1ld2bqq3r one-third  valign-top clearfix" style="padding:0 2%">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-ixxejw4f3 one column_icon_box">
                                                <div class="icon_box icon_position_top no_border ">

                                                    <center>
                                                        <img src="{!! asset('pagina/img/ico3.png') !!}">
                                                    </center>


                                                    <div class="desc_wrapper">
                                                        <h4 class="title">INFOGRAFÍAS</h4>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="section mcb-section mcb-section-lf83x8pnr  dark  bg-cover bg-cover" style="padding-top:90px;padding-bottom:90px;background-image:url({!! asset('pagina/img/slider2.png') !!});background-repeat:no-repeat;background-position:center;background-attachment:fixed;background-size:cover;-webkit-background-size:cover">
                                <div class="section_wrapper mcb-section-inner">
                                    <div class="wrap mcb-wrap mcb-wrap-n1b3n8szh one  valign-top clearfix">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-mc8c4de4u one-third  column_column  ">
                                                &nbsp;
                                            </div>
                                            <div class="column mcb-column mcb-item-mc8c4de4u two-third  column_column  ">
                                                <div class="column_attr clearfix align_center" style>

                                                    <h2 style="font-size: 38px;
                                                        line-height: 45px;
                                                        font-weight: 200;
                                                        letter-spacing: 0px;">Forma parte de la <br>
                                                        comunidad médica informada,<br>te invitamos a unirte</h2>

                                                    <a href="{{ url('/register') }}"  style=" white-space: nowrap; font-size: 20px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Nunito;background-color:rgb(15,40,84);border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; padding: 10px 30px 10px 30px" >Regístrate aquí</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="section mcb-section mcb-section-949o9civf" id="services" style="padding-top:30px;padding-bottom:0px;background-color:#242f44">
                                <div class="section_wrapper mcb-section-inner">
                                    <div class="wrap mcb-wrap mcb-wrap-qz1a7y1v0 one  valign-top clearfix">
                                        <div class="mcb-wrap-inner">

                                            <div class="column mcb-column mcb-item-ksw4sgsdb one column_column">
                                                <div class="column_attr clearfix align_center" style>
                                                    <h2 style="color: #fff" >BENEFICIOS</h2>
                                                    <h3 style="color: #fff">relacionados con tu especialidad.</h3>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="wrap mcb-wrap mcb-wrap-mua25bd7v one-third  valign-top clearfix" style="padding:0 1%">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-lvrpzd9dp one column_icon_box">
                                                <div class="icon_box icon_position_top no_border">
                                                    <a class="scroll" href="#gallery">
                                                        <div class="image_wrapper"><img src="{!! asset('pagina/img/ico4.png') !!}" class="scale-with-grid">
                                                        </div>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap mcb-wrap mcb-wrap-5c932bifg one-third  valign-top clearfix" style="padding:0 1%">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-m8u5rh7cl one column_icon_box">
                                                <div class="icon_box icon_position_top no_border">
                                                    <a class="scroll" href="#gallery">
                                                        <div class="image_wrapper"><img src="{!! asset('pagina/img/ico5.png') !!}" class="scale-with-grid">
                                                        </div>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap mcb-wrap mcb-wrap-fi59hpr6g one-third  valign-top clearfix" style="padding:0 1%">
                                        <div class="mcb-wrap-inner">
                                            <div class="column mcb-column mcb-item-hhhafp12j one column_icon_box">
                                                <div class="icon_box icon_position_top no_border">
                                                    <a class="scroll" href="#gallery">
                                                        <div class="image_wrapper"><img src="{!! asset('pagina/img/ico6.png') !!}" class="scale-with-grid">
                                                        </div>

                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>



                            <div class="section mcb-section mcb-section-949o9civf" id="services" style="padding-top:30px;padding-bottom:0px;background-color:#fff">
                                <div class="section_wrapper mcb-section-inner">
                                    <div class="wrap mcb-wrap mcb-wrap-qz1a7y1v0 one  valign-top clearfix">
                                        <div class="mcb-wrap-inner">

                                            <div class="column mcb-column mcb-item-ksw4sgsdb one column_column">
                                                <div class="column_attr clearfix align_center" >
                                                    <a href="{{ url('/register') }}">
                                                        <img src="{!! asset('pagina/img/banner11.png') !!}">
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>





                            <div class="section mcb-section mcb-section-lf83x8pnr  dark  bg-cover " style="padding-top:90px;padding-bottom:90px;background-image:url({!! asset('pagina/img/banner12.png') !!});background-repeat:no-repeat;background-position:center;background-size:cover;-webkit-background-size:cover">
                                <div class="section_wrapper mcb-section-inner">
                                    <div class="wrap mcb-wrap mcb-wrap-n1b3n8szh one  valign-top clearfix">
                                        <div class="mcb-wrap-inner">

                                            <div class="column mcb-column mcb-item-mc8c4de4u two-third  column_column  ">
                                                <div class="column_attr clearfix align_center" style>

                                                    <h2 style="font-size: 38px;
                                                        line-height: 45px;
                                                        font-weight: 200;
                                                        letter-spacing: 0px;">Obtén puntos de certificación <br>
                                                        con los cursos que tomes</h2><br>

                                                    <a href="{{ url('/register') }}"  style=" white-space: nowrap; font-size: 20px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Nunito;background-color:rgb(0,158,226);border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; padding: 10px 30px 10px 30px" >Regístrate aquí</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <!--
                             <div class="section mcb-section mcb-section-9w4om4i9s" id="work" style="padding-top:40px;padding-bottom:25px;background-color:#faf9fe;">
                                  <div class="section_wrapper mcb-section-inner">
                                       <div class="wrap mcb-wrap mcb-wrap-8545a4r3b one  valign-top clearfix">
                                            <div class="mcb-wrap-inner">

                                                 <div class="column mcb-column mcb-item-aahg8szpi one column_column column-margin-10px">
                                                      <div class="column_attr clearfix align_center" style>
                                                           <h2 style="color: #556c98;">NUESTROS CURSOS</h2>
                                                           <h3 style="color:#556c98;">Explora todos los cursos que eduPlace tiene para ti</h3>
                                                      </div>
                                                 </div>

                                            </div>
                                       </div>
                                       <div class="wrap mcb-wrap mcb-wrap-816gse6yc two-third  valign-top clearfix" style="padding:0 2%">
                                            <div class="mcb-wrap-inner">
                                                 <div class="column mcb-column mcb-item-1icjlcvbz one column_image">
                                                      <img src="{!! asset('pagina/img/bannerc1.png') !!}">
                                                 </div>
                                            </div>
                                       </div>
                                       <div class="wrap mcb-wrap mcb-wrap-87gbte1zy one-third  valign-top clearfix" style="padding:0 2%">
                                            <div class="mcb-wrap-inner">
                                                 <div class="column mcb-column mcb-item-n1x859fk1 one column_image">
                                                      <img src="{!! asset('pagina/img/bannerc2.png') !!}">
                                                 </div>
                                            </div>
                                       </div>

                                  </div>
                             </div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
            <footer id="Footer" class="clearfix" style="padding-top:20px;">
                <div class="footer_copy">
                    <div class="container">

                        <div class="column three-fifth">

                            <!--
                             <ul class="social">
                                  <li class="facebook">
                                       <a href="#" title="Facebook"><i class="icon-facebook"></i></a>
                                  </li>
                                  <li class="youtube">
                                       <a href="#" title="YouTube"><i class="icon-play"></i></a>
                                  </li>
                                  <li class="pinterest">
                                       <a href="#" title="Pinterest"><i class="icon-pinterest"></i></a>
                                  </li>
                                  <li class="instagram">
                                       <a href="#" title="Instagram"><i class="icon-instagram"></i></a>
                                  </li>
                             </ul>
                            -->

                            <br>
                            <br>
                            <br>
                            <br>


                            <img src="{!! asset('pagina/img/logo3.png') !!}"><br>
                            <span style="font-size: 16px; color: #30527b; font-weight: 300;">&copy; 2020 eduPlace. Todos los derechos reservados.</span>


                        </div>
                        <div class="column one-fifth">



                        </div>
                        <div class="column one-fifth">
                            <h4 style="font-size: 20px;line-height: 29px; font-weight: 200; letter-spacing: 0px; color: #4d6184;">MÁS</h4>
                            <a href="{!! url('aviso_privacidad_eduplace') !!}" target="_blank" style="color:#fff">Aviso de Privacidad</a><br>
                            <a href="{!!  url('legales') !!} " target="_blank" style="color:#fff">Legales</a><br>
                            Contacto<br>
                            <a href="mailto:contacto@eduplace.com.mx">contacto@eduplace.com.mx</a><br>
                            <a id="back_to_top" class="button button_js" href><i class="icon-up-open-big"></i></a>
                        </div>


                    </div>
                </div>
            </footer>
        </div>


        <!-- JS -->
        <script src="{!! asset('pagina/js/jquery-2.1.4.min.js') !!}"></script>

        <script src="{!! asset('pagina/js/mfn.menu.js') !!}"></script>
        <script src="{!! asset('pagina/js/jquery.plugins.js') !!}"></script>
        <script src="{!! asset('pagina/js/jquery.jplayer.min.js') !!}"></script>
        <script src="{!! asset('pagina/js/animations/animations.js') !!}"></script>
        <script src="{!! asset('pagina/js/translate3d.js') !!}"></script>
        <script src="{!! asset('pagina/js/scripts.js') !!}"></script>
        <script src="{!! asset('pagina/js/email.js') !!}"></script>

        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/jquery.themepunch.tools.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/jquery.themepunch.revolution.min.js') !!}"></script>

        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.video.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.slideanims.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.actions.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.layeranimation.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.kenburn.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.navigation.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.migration.min.js') !!}"></script>
        <script src="{!! asset('pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.parallax.min.js') !!}"></script>

        <script>
                    var revapi1, tpj;
            (function () {
                if (!/loaded|interactive|complete/.test(document.readyState))
                    document.addEventListener("DOMContentLoaded", onLoad);
                else
                    onLoad();
                function onLoad() {
                    if (tpj === undefined) {
                        tpj = jQuery;
                        if ("off" == "on")
                            tpj.noConflict();
                    }
                    if (tpj("#rev_slider_1_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                    } else {
                        revapi1 = tpj("#rev_slider_1_1").show().revolution({
                            sliderType: "standard",
                            sliderLayout: "auto",
                            dottedOverlay: "none",
                            delay: 3000,
                            navigation: {
                                onHoverStop: "off",
                            },
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: 1080,
                            gridheight: 768,
                            lazyType: "none",
                            shadow: 0,
                            spinner: "spinner2",
                            stopLoop: "off",
                            stopAfterLoops: -1,
                            stopAtSlide: -1,
                            shuffle: "off",
                            autoHeight: "off",
                            disableProgressBar: "on",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: false,
                            }
                        });
                    }
                    ;
                }
                ;
            }());
        </script>


    </body>

</html>
