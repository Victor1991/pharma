
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Login -  </title>


     <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/estilos/estilos.css') !!}" />

     <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155648111-1"></script>
    <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-155648111-1');
     </script>

</head>
<body class="gray-bg fondo_azul">

     <!-- Wrapper-->
     <div class="middle-box text-center loginscreen animated fadeInDown" style="padding-top: 1%;">
          <div>
               <div class=" p-md">
                    <div>
                         <h1 class="logo-name"><img src="{!! asset('img/logologin.png') !!}" alt="eduPlace" class="img-responsive img-login"></h1>
                    </div>

                    <h2 style="font-weight: 600; font-size: 16px; color: #fff;">Gracias por su registro</h2>
                    <br>
                    <p style="color:#fff;">

                        Ya puedes iniciar sesión con el usuario registrado.

                    </p>
                    <br>

                    <a class="btn btn-primary btn-rounded btn-block" id="ip3" href="{{ url('/') }}">Volver al inicio</a>
               </div>
          </div>
     </div>
     <!-- End wrapper-->

</body>
</html>
