<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta http-equiv="content-type" content="application/vnd.ms-excel;" charset="UTF-8">

     <title></title>

     <!-- Bootstrap -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">





     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
</head>
<body>

     <!------ Include the above in your HEAD tag ---------->

     <div class="container">
          <br>
          <div class="row">
               <div class="jumbotron">
                    <h1>{{ $curso['nombre']}}</h1>
               </div>
          </div>

          <div class="row">
               <div class="panel panel-default">
                    <div class="panel-heading">
                         <h3 style="margin-top: 10px;"> Rangos de edades </h3>
                         <button class="btn btn-info pull-left" style="float: right !important; margin-top: -40px;" onclick="exportTableToExcel('edades')">Descargar</button>

                    </div>
                    <div class="panel-body">
                         <div class="table-responsive">
                              <table class="table  table-striped table-bordered" id="edades">
                                   <thead>
                                        <tr>
                                             <th>Edad</th>
                                             <th>Usuarios</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        <?php foreach ($edades as $key => $edad): ?>
                                             <tr>
                                                  <th>{{ $key }} </th>
                                                  <th>{{ $edad }} usuario(s)</th>
                                             </tr>
                                        <?php endforeach; ?>
                                   </tbody>
                              </table>
                         </div>
                    </div>
               </div>
          </div>


          <div class="row">
               <div class="panel panel-default">
                    <div class="panel-heading">
                         <h3 style="margin-top: 10px;"> El porcentaje de participación en sexo </h3>
                         <button class="btn btn-info pull-left" style="float: right !important; margin-top: -40px;" onclick="exportTableToExcel('sexo')">Descargar</button>
                    </div>
                    <div class="panel-body">
                         <div class="table-responsive">
                              <table class="table table-striped table-bordered" id="sexo">
                                   <thead>
                                        <tr>
                                             <th>Sexo</th>
                                             <th>Usuarios</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        <?php foreach ($sexo as $key => $usuarios): ?>
                                             <tr>
                                                  <?php if ($key == ''): ?>
                                                       <th>No registrado</th>
                                                  <?php else: ?>
                                                       <th>{{ $key }}</th>
                                                  <?php endif; ?>
                                                  <th>{{ $usuarios }} usuario(s)</th>
                                             </tr>
                                        <?php endforeach; ?>
                                   </tbody>
                              </table>
                         </div>
                    </div>
               </div>
          </div>
          <div class="row">
               <div class="panel panel-default">
                    <div class="panel-heading">
                         <h3 style="margin-top: 10px;"> Resultado de participantes </h3>
                         <!-- <button class="btn btn-info pull-left" style="float: right !important; margin-top: -40px;" onclick="exportTableToExcel('report')">Descargar</button> -->
                         <!-- <p><button id="exportexcel">Export</button></p> -->


                         <input id="exportexcel" type="button" class="btn btn-info pull-left" style="float: right !important; margin-top: -40px;" onclick="tableToExcel('tblExport', 'W3C Example Table')" value="Descargar">

                    </div>
                    <div class="panel-body">
                         <div class="table-responsive">
                              <table class="table table-bordered a"  id="table2excel">
                                   <thead>
                                        <tr>
                                             <th colspan="6">Usurio</th>
                                             <th colspan="4">Intento</th>
                                             <?php if ($preguntas_pre): ?>
                                                  <th colspan="{{ count($preguntas_pre) }}">Pre examen</th>
                                             <?php endif; ?>
                                             @foreach($materiales as $key => $mat)
                                                  <th colspan="{{ $mat['material']['n_preguntas'] }}">{{ $mat['material']['titulo'] }}</th>
                                             @endforeach
                                             <?php if ($preguntas_post): ?>
                                                  <th colspan="{{ count($preguntas_post) }}">Post examen</th>
                                             <?php endif; ?>

                                        </tr>
                                        <tr>
                                             <th>Id</th>
                                             <th>Nombre Completo</th>
                                             <th>Correo</th>
                                             <th>Edad</th>
                                             <th>Sexo</th>

                                             <th>País</th>
                                             <th>Ciudad</th>
                                             <th>Estado</th>

                                             <th>Instituto</th>
                                             <th>Especialidad</th>

                                             <th>Cédula</th>


                                             <th>Calificación</th>
                                             <th>Intento</th>
                                             <th>Materiales</th>
                                             <th>Materiales tomados</th>
                                             <th>Finalizo</th>
                                             @foreach ($preguntas_pre as $key => $pregunt)
                                             <th>{{ $pregunt->titulo }}</th>
                                             @endforeach
                                             @foreach($materiales as $key => $mat)
                                                  @foreach($mat['pregunta'] as $key => $pre)
                                                  <th>{{ $pre['titulo'] }}</th>
                                                  @endforeach
                                             @endforeach
                                             @foreach ($preguntas_post as $key => $pregunt)
                                             <th>{{ $pregunt->titulo }}</th>
                                             @endforeach
                                        </tr>
                                   </thead>
                                   <tbody>
                                        <?php foreach ($alumnos as $key => $alumno): ?>

                                             <?php $int = 1; foreach ($alumno['intentos'] as $key1 => $intentos): ?>
                                                  <tr>
                                                       <td>{{ $alumno['alumno']->id }}</td>
                                                       <td>{{ $alumno['alumno']->nombre}} {{ $alumno['alumno']->apellido_paterno}} {{$alumno['alumno']->apellidos_materno }} </td>
                                                       <td>{{ $alumno['alumno']->correo }}</td>
                                                       <td>{{ $alumno['alumno']->edad }}</td>
                                                       <td>{{ $alumno['alumno']->sexo }}</td>

                                                       <td>
                                                            <?php if (isset($alumno['alumno']->pais)): ?>
                                                                 {{ $alumno['alumno']->pais }}
                                                            <?php endif; ?>
                                                       </td>

                                                       <td>
                                                            <?php if (isset($alumno['alumno']->ciudad)): ?>
                                                                 {{ $alumno['alumno']->ciudad }}
                                                            <?php endif; ?>
                                                       </td>
                                                       <td>
                                                            <?php if (isset($alumno['alumno']->estado)): ?>
                                                                 {{ $alumno['alumno']->estado }}
                                                            <?php endif; ?>
                                                       </td>

                                                       <td>{{ $alumno['alumno']->instituto }}</td>
                                                       <td>{{ $alumno['alumno']->especialidad }}</td>
                                                       <td>
                                                            <?php if (isset($alumno['alumno']->estado)): ?>
                                                                 {{ $alumno['alumno']->cedula }}
                                                            <?php endif; ?>
                                                       </td>


                                                       <td>{{ $intentos['calificacion'] }}</td>
                                                       <td>{{ $int }}</td>
                                                       <td>{{ $intentos['total_materiales'] }}</td>
                                                       <td>{{ $intentos['materiales_finalizados'] }}</td>
                                                       <td>
                                                            <?php if ($intentos['total_materiales'] == $intentos['materiales_finalizados']): ?>
                                                                 Si
                                                            <?php else: ?>
                                                                 No
                                                            <?php endif; ?></th>
                                                       </td>
                                                       <?php if($preguntas_pre): ?>
                                                            @foreach ($intentos['pre_respuetas'] as $pr_resp)
                                                            <?php if ($pr_resp): ?>
                                                                 <td>
                                                                      {{  $pr_resp->opcion  }}
                                                                 </td>
                                                                 <?php else: ?>
                                                                      <td></td>
                                                                 <?php endif; ?>
                                                            @endforeach
                                                       <?php endif; ?>

                                                       @foreach ($intentos['material'] as $mt)
                                                       <?php if ($mt): ?>
                                                            <td>
                                                                 <?php if ($mt->opcion): ?>
                                                                      {{  $mt->opcion  }}
                                                                 <?php else: ?>
                                                                      {{  $mt->opcion  }}
                                                                 <?php endif; ?>
                                                            </td>
                                                       <?php else: ?>
                                                            <td></td>
                                                       <?php endif; ?>
                                                       @endforeach

                                                       <?php if($preguntas_post): ?>
                                                            @foreach ($intentos['post_respuetas'] as $pr_resp)
                                                            <?php if ($pr_resp): ?>
                                                                 <td>
                                                                      {{  $pr_resp->opcion  }}
                                                                 </td>
                                                            <?php else: ?>
                                                                 <td></td>
                                                            <?php endif; ?>
                                                            @endforeach
                                                       <?php endif; ?>


                                                  </tr>
                                                  <?php $int++; endforeach;?>
                                             <?php endforeach;?>
                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    </div>
               </div>

               <div class="row">
                    <div class="panel panel-default">
                         <div class="panel-heading">
                              <h3 style="margin-top: 10px;"> Usurio sin empezar </h3>
                              <!-- <button class="btn btn-info pull-left" style="float: right !important; margin-top: -40px;" onclick="exportTableToExcel('sin_inicio')">Descargar</button> -->
                              <input type="button" class="btn btn-info pull-left" style="float: right !important; margin-top: -40px;" onclick="tableToExcel('sin_inicio', 'W3C Example Table')" value="Descargar">

                         </div>
                         <div class="panel-body">
                              <div class="table-responsive">
                                   <table class="table  table-striped table-bordered" id="sin_inicio">
                                        <thead>
                                             <tr>
                                                  <th>#</th>
                                                  <th>Nombres</th>
                                                  <th>Apellidos</th>
                                                  <th>Correo</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <?php foreach ($usuarios_sin_empezar as $key => $usuarios): ?>
                                                  <tr>
                                                       <th>{{ $key+1 }} </th>
                                                       <th>{{ $usuarios->nombre }}</th>
                                                       <th>{{ $usuarios->apellido_paterno }} {{ $usuarios->apellidos_materno }}</th>
                                                       <th>{{ $usuarios->correo }}</th>
                                                  </tr>
                                             <?php endforeach; ?>
                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    </div>
               </div>


          </div>




          <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
          <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
          <!-- Include all compiled plugins (below), or include individual files as needed -->
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

          <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


          <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>




          <script>

          $("#exportexcel").click(function(){
            $("#table2excel").table2excel({
              // exclude CSS class
              exclude:".noExl",
              name:"Worksheet Name",
              filename:"SomeFile",//do not include extension
              fileext:".xls" // file extension
            });
          });


          // $(document).ready(function(){
          //   var table = $('#example').DataTable();
          //
          //   $('#btn-export').on('click', function(){
          //       $('<table>').append(table.$('tr').clone()).table2excel({
          //           exclude: ".excludeThisClass",
          //           name: "Worksheet Name",
          //           filename: "SomeFile" //do not include extension
          //       });
          //   });
          // })

          $(document).ready(function() {
              $('#example').DataTable( {
                  dom: 'Bfrtip',
                  buttons: [
                      'copy', 'csv', 'excel', 'pdf', 'print'
                  ]
              } );
          } );


          var tableToExcel = (function() {
          var uri = 'data:application/vnd.ms-Excel;base64,'
            , template = '<html xmlns:o="urn:schemas-Microsoft-com:office:office" xmlns:x="urn:schemas-Microsoft-com:office:Excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-Excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
          return function(table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
            window.location.href = uri + base64(format(template, ctx))
          }
        })()

          function exportTableToExcel(tableID, filename = ''){
               var downloadLink;
               var dataType = 'application/vnd.ms-excel';
               var tableSelect = document.getElementById(tableID);
               var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

               // Specify file name
               filename = filename?filename+'.xls':'excel_data.xls';

               // Create download link element
               downloadLink = document.createElement("a");

               document.body.appendChild(downloadLink);

               if(navigator.msSaveOrOpenBlob){
                    var blob = new Blob(['\ufeff', tableHTML], {
                         // type: dataType
                         type: "text/plain;charset=utf-8;"
                    });
                    navigator.msSaveOrOpenBlob( blob, filename);
               }else{
                    // Create a link to the file
                    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                    // Setting the file name
                    downloadLink.download = filename;

                    //triggering the function
                    downloadLink.click();
               }
          }
          </script>
          <script>
          $(document).ready(function() {
               $('#participantes').DataTable({
                    "pageLength": 50,
                    dom: 'Bfrtip',
                       buttons: [
                         'csv', 'excel'
                       ]
               });

          } );
          </script>
          <style media="screen">
          #reportPlace table /*thead tr*/ {
               display: block;
          }

          #reportPlace table tbody {
               /*    display: block;*/
               height: 262px;
               overflow: auto;
          }

          </style>

     </body>
     </html>
