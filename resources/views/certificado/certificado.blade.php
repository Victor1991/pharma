<!DOCTYPE html>
<html>

<head>

     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <title>Certificado </title>

     <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

     <!-- Toastr style -->
     <!-- Styles -->
     <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
</head>

<body>
     <div id="wrapper">
          <div class="row">
               <div class="wrapper wrapper-content  animated fadeInRight article">
                    <div class="row justify-content-md-center">
                         <div class="col-md-offset-4 col-md-4">
                              <div class="ibox">
                                   <div class="ibox-content"  style="">
                                        <div class="col-md-12">
                                             <div class="row text-center">
                                                  <div class="div_nombre">

                                                       <H2 class="nombre"> {{ $usuario->nombre. ' ' . $usuario->apellido_paterno. ' ' . $usuario->apellidos_materno }}</H2>

                                                  </div>

                                             </div>

                                        </div>

                                        <div class="clearfix">

                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>

     <style media="screen">
     .ibox-content{
          background-image: url({!! asset('img/'.$curso->img_certificado) !!});
          background-repeat: no-repeat;
          background-size: 100% 100%;
          width:800px;
          height: 600px;
     }
     .div_nombre{
          margin-top: 12%;
          font-size: 20px;
          font-weight: 500;
     }
     .nombre{
          font-size: 25px;
         color: #2b7e9c;
         font-weight: 600;
     }
     </style>

</body>
</html>
