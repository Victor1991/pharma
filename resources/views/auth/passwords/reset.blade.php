<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">

     <title>{{ config('app.name', 'Laravel') }}</title>

     <!-- Styles -->
     <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

     <!-- Sweet alert  -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />


     <!-- Scripts -->
     <script>
     window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
     ]) !!};
     </script>

     <script src="{!! asset('js/countdown.js') !!}" type="text/javascript"></script>

</head>
<body class="top-navigation">

     <!-- Wrapper-->
     <div id="wrapper">

          <!-- Page wraper -->
          <div id="page-wrapper" class="gray-bg">
               <div class="container">
                    <div class="row">
                         <div class="col-md-8 col-md-offset-2">
                              <div class="panel panel-default">
                                   <div class="panel-heading">Reset Password</div>

                                   <div class="panel-body">
                                        @if (session('status'))
                                        <div class="alert alert-success">
                                             {{ session('status') }}
                                        </div>
                                        @endif

                                        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                                             {{ csrf_field() }}

                                             <input style="opacity: 1;" type="hidden" name="token" value="{{ $token }}">

                                             <div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
                                                  <label for="correo" class="col-md-4 control-label">E-Mail Address</label>

                                                  <div class="col-md-6">
                                                       <input style="opacity: 1;" id="correo" type="correo" class="form-control" name="correo" value="{{ $correo or old('correo') }}" required autofocus>

                                                       @if ($errors->has('correo'))
                                                       <span class="help-block">
                                                            <strong>{{ $errors->first('correo') }}</strong>
                                                       </span>
                                                       @endif
                                                  </div>
                                             </div>

                                             <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                  <label for="password" class="col-md-4 control-label">Password</label>

                                                  <div class="col-md-6">
                                                       <input style="opacity: 1;" id="password" type="password" class="form-control" name="password" required>

                                                       @if ($errors->has('password'))
                                                       <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                       </span>
                                                       @endif
                                                  </div>
                                             </div>

                                             <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                                  <div class="col-md-6">
                                                       <input style="opacity: 1;" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                                       @if ($errors->has('password_confirmation'))
                                                       <span class="help-block">
                                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                       </span>
                                                       @endif
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <div class="col-md-6 col-md-offset-4">
                                                       <button type="submit" class="btn btn-primary">
                                                            Reset Password
                                                       </button>
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</body>
</html>
