<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">

     <title>{{ config('app.name', 'Laravel') }}</title>

     <!-- Styles -->
     <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

     <!-- Sweet alert  -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />


     <!-- Scripts -->
     <script>
     window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
     ]) !!};
     </script>

     <script src="{!! asset('js/countdown.js') !!}" type="text/javascript"></script>

</head>
<body class="top-navigation">

     <!-- Wrapper-->
     <div id="wrapper">

          <!-- Page wraper -->
          <div id="page-wrapper" class="gray-bg">
               <div class="container">
                    <div class="row">
                         <div class="col-md-8 col-md-offset-2">
                              <div class="panel panel-default" style="margin-top: 12%;">

                                   <div class="panel-body">
                                        @if (session('status'))
                                        <div class="alert alert-success">
                                             {{ session('status') }}
                                        </div>
                                        @endif

                                        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                                             {{ csrf_field() }}

                                             <div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">

                                                  <div class="row text-center">
                                                       <h2>Recuperar contraseña</h2>
                                                  </div>
                                                  <br>

                                                  <label for="correo" class="col-md-4 control-label">Correo </label>

                                                  <div class="col-md-6">
                                                       <input id="correo" type="email" class="form-control" name="correo"  value="{{ old('correo') }}" style="opacity: 1;" required>

                                                       @if ($errors->has('correo'))
                                                       <span class="help-block">correo
                                                            <strong>{{ $errors->first('correo') }}</strong>
                                                       </span>
                                                       @endif
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <div class="col-md-6 col-md-offset-4">
                                                       <button type="submit" class="btn btn-primary">
                                                            Enviar contraseña Restablecer enlace
                                                       </button>
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</body>
</html>
