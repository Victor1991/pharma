<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Registro - @yield('title') </title>

     <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
          <link rel="stylesheet" href="{!! asset('css/estilos/estilos.css') !!}" />

</head>

<body class="gray-bg  ">

     <!-- Wrapper-->
     <div class="middle-box text-center loginscreen animated fadeInDown box-registre"  style="padding-top: 1%;">
          <div>
               <div class=" p-md ">
                    <div>
                         <h1 class="logo-name"><img src="{!! asset('img/logologin.png') !!}" alt="logo" class="img-responsive img-login"></h1>
                    </div>

                    <h2 style="font-weight: 600; font-size: 16px; color: #556c98;">Crear cuenta</h2>
                    <br>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                         {{ csrf_field() }}
                         <input type="hidden" name="curso_id" value="{{ $curso_id }}">

                         <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="nombre" placeholder="Nombre(s)*" type="text" class="form-control ip2" name="nombre" value="{{ old('nombre') }}" required autofocus>
                                   @if ($errors->has('nombre'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('apellido_paterno') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="apellido_paterno" placeholder="Apellido Paterno*" type="text" class="form-control ip2" name="apellido_paterno" value="{{ old('apellido_paterno') }}" required autofocus>
                                   @if ($errors->has('apellido_paterno'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('apellido_paterno') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('apellidos_materno') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="apellidos_materno" placeholder="Apellido Materno*" type="text" class="form-control ip2" name="apellidos_materno" value="{{ old('apellidos_materno') }}" required autofocus>
                                   @if ($errors->has('apellidos_materno'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('apellidos_materno') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }}" >
                              <div class="col-md-12">
                                   <select class="form-control "  name="sexo" id="sexo" required autofocus>
                                        <option value="" selected disabled hidden>Sexo *</option>
                                        <option {{ ( old('sexo') ==  'masculino') ? 'selected' : '' }} value="masculino" > Masculino</option>
                                        <option {{ ( old('sexo') ==  'femenino') ? 'selected' : '' }} value="femenino" > Femenino </option>
                                   </select>
                                   @if ($errors->has('sexo'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('sexo') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('edad') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <select class="form-control"  name="edad" required autofocus >
                                        <option value="" selected disabled hidden> Edad *</option>
                                        <?php for($i=15; $i < 99 ; $i++){ ?>
                                             <option {{ ( old('edad') ==  $i) ? 'selected' : '' }} value="<?=$i;?>"><?=$i;?></option>
                                        <?php } ?>
                                   </select>
                                   @if ($errors->has('edad'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('edad') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <select class="form-control" name="pais" id="pais" required autofocus onchange="getval(this);">
                                        <option value="" selected disabled hidden>País *</option>
                                        @foreach ($pais as $pais)
                                        <option {{ ( old('pais') == $pais['id']) ? 'selected' : '' }} value="{{ $pais['id'] }}" > {{ $pais['nombre'] }}</option>
                                        @endforeach
                                   </select>
                                   @if ($errors->has('pais'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>


                         <div id="select_estados" class="form-group{{ $errors->has('estados') ? ' has-error' : '' }}" <?php if(old('pais') != 117): ?> style="display:none;" <?php endif;?>>
                              <div class="col-md-12">
                                   <select class="form-control" name="estados" id="estados" >
                                        <option value="" selected disabled hidden>Estado *</option>
                                        @foreach ($estados as $estados)
                                        <option {{ ( old('estados') == $estados['id']) ? 'selected' : '' }} value="{{ $estados['id'] }}" > {{ $estados['nombre'] }}</option>
                                        @endforeach
                                   </select>
                                   @if ($errors->has('estados'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('estados') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}"  >
                              <div class="col-md-12">
                                   <input id="ciudad" placeholder="Ciudad " type="text" class="form-control ip2" name="ciudad" value="{{ old('ciudad') }}" autofocus >
                                   @if ($errors->has('ciudad'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('ciudad') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>


                         <div class="form-group{{ $errors->has('codigo_postal') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="codigo_postal" placeholder="Código Postal " type="text" class="form-control ip2" name="codigo_postal" value="{{ old('codigo_postal') }}" required autofocus>
                                   @if ($errors->has('codigo_postal'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('codigo_postal') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('cedula_profecional') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="cedula_profecional" placeholder="Cédula Profesional* " type="text" class="form-control ip2" name="cedula_profecional" value="{{ old('cedula_profecional') }}" required autofocus>
                                   @if ($errors->has('cedula_profecional'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('cedula_profecional') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('especialidad') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <select class="form-control" name="especialidad" id="especialidad" required autofocus >
                                        <option value="" selected disabled hidden>Especialidad *</option>
                                        @foreach ($especialidades as $especialidad)
                                        <option {{ ( old('especialidad') == $especialidad['id']) ? 'selected' : '' }} value="{{ $especialidad['id'] }}" > {{ $especialidad['nombre'] }}</option>
                                        @endforeach
                                   </select>
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('institucion') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <!-- <input id="institucion" placeholder="Institución* " type="text" class="form-control ip2" name="institucion" value="{{ old('institucion') }}" required autofocus> -->

                                   <select class="form-control" name="institucion" id="institucion" required autofocus >
                                        <option value="" selected disabled hidden> Institución* </option>
                                        @foreach ($instituciones as $instituciones)
                                        <option {{ ( old('institucion') == $instituciones['id']) ? 'selected' : '' }} value="{{ $instituciones['id'] }}" > {{ $instituciones['nombre'] }}</option>
                                        @endforeach
                                   </select>
                                   @if ($errors->has('institucion'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('institucion') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('residentes') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <select class="form-control" name="residentes" id="residentes"  autofocus >
                                        <option value="" selected disabled hidden> Solo para residentes </option>
                                        @foreach ($residentes as $residentes)
                                        <option {{ ( old('residentes') == $residentes['id']) ? 'selected' : '' }} value="{{ $residentes['id'] }}" > {{ $residentes['nombre'] }}</option>
                                        @endforeach
                                   </select>
                                   @if ($errors->has('pais'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="celular" placeholder="Número de celular* " type="text" class="form-control ip2" name="celular" value="{{ old('celular') }}" required autofocus>
                                   @if ($errors->has('celular'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('celular') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div>
                              <div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
                                   <div class="col-md-12">
                                        <input id="correo" placeholder="Correo* " type="text" class="form-control ip2" name="correo" value="{{ $correo or old('correo') }}" required autofocus>
                                        @if ($errors->has('correo'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('correo') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>
                         </div>

                         <div class="form-group">
                              <div class="col-md-12">
                                  <input id="correo-confirm" type="text" class="form-control ip2" placeholder="Confirme correo*"  name="correo_confirmation" required autofocus>
                              </div>
                         </div>


                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                   <input id="password" type="password" class="form-control ip2" name="password" placeholder="Contraseña" required autofocus>
                                   @if ($errors->has('password'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>

                         <div class="form-group">

                              <div class="col-md-12">
                                  <input id="password-confirm" type="password" class="form-control ip2" placeholder="Confirme Contraseña*" name="password_confirmation" required autofocus><br>
                                  <span style="font-size: 11px; text-align: left;">(*) Campos obligatorios</span>
                              </div>
                         </div>

                         <div class="form-group{{ $errors->has('terminos') ? ' has-error' : '' }}">
                              <input type="checkbox" name="terminos" value="1"> He leido y acepto los Términos y Condiciones de Uso
                              @if ($errors->has('terminos'))
                              <span class="help-block">
                                   <strong> Es necesario aceptar los términos y condiciones.</strong>
                              </span>
                              @endif
                         </div>

                         <div class="form-group">
                              <div class="col-md-12">
                                   <button type="submit" class="btn btn-primary btn-block"  id="ip3">
                                        Regístrate ahora
                                   </button>
                              </div>
                              <div class="col-md-12 m-t-sm ">Si ya tienes una cuenta
                                   <a class=""  href="javascript:history.back()">
                                        inicia sesión
                                   </a><br><br>

                              </div>
                         </div>
                    </form>
               </div>
          </div>
          <span style="font-size: 11px; ">
              Para obtener más información sobre cómo <strong>eduPlace</strong> recopila,<br>
              utiliza, comparte y protege sus datos personales, <br>
              lea el <a href="{!! url('aviso_privacidad_eduplace') !!}" target="_blank"> Aviso de Privacidad</a> de <strong>eduPlace</strong>.
          </span><br><br><br>
     </div>

     <!-- End wrapper-->
     <script>
     function getval(sel)
     {
          if (sel.value == 117) {
               $('#select_estados').show();
               $("#estados").prop('required', true);
          }else {
               $("#estados").prop('required', false);
               $('#select_estados').hide();
               $('#estados').val('');
          }
     }
     </script>
     <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

     @section('scripts')
     @show

</body>
</html>
