@extends('layouts.app')

@section('title', 'Main page')

@section('content')

<!-- Page heading -->
<!--<div class="row wrapper border-bottom bg-info page-heading ">
     <div class="container m-t-md">
          <div class="col-md-12">
               <h1>Evalución</h1>
          </div>
     </div>
</div>-->


<div class="container m-t-md ">

     <!-- Cursos -->
     <div class="col-md-8">
          <h2>{{ $examen['titulo'] }}</h2>
          <h3>{!! $examen['descripcion'] !!}</h3>
          <br>
          <div class="ibox">
               <div class="ibox-content">
                    <form id="regForm" method="post" action="{{ route('calificar_examen', ['id' => $examen['id']]) }}" onsubmit="return checkform();">
                         <input type="hidden" name="f_inicio" value="{{ date('Y-m-d h:i:s') }}">
                         @foreach ($preguntas as $key => $pregunta)
                         <div class="tab">
                              {!! $pregunta['pregunta']['descripcion'] !!}
                              <h4>{{ $pregunta['pregunta']['titulo'] }}</h4>

                              <br>
                              <div class="row">
                                   @foreach ($pregunta['respuesta'] as $key2 => $respuestas)
                                        <div class="col-md-12 ">
                                             <label class="container">
                                                  <p style="margin-left:16px; font-size: 15px; text-align: justify;"><?=$respuestas['opcion']?></p>
                                                    <input type="radio" class="resp_especialkey" mensaje="{{ $respuestas['descripcion'] }}" pregunta="{{ $pregunta['pregunta']['id'] }}"  name="pregunta[{{ $pregunta['pregunta']['id'] }}]" value="{{ $respuestas['id'] }}" required>
                                               <span class="checkmark"></span>
                                             </label>
                                        </div>
                                   @endforeach
                              </div>
                         </div>
                         @endforeach
                         <br>
                         <div style="overflow:auto;">
                              <div style="float:right;">
                                   <button type="button" id="prevBtn"  onclick="nextPrev(-1)">Anterior</button>
                                   <button type="button" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                              </div>
                         </div>
                         <!-- Circles which indicates the steps of the form: -->
                    </form>
               </div>
          </div>
     </div>

     <!-- Temporalizadors  -->
     <div class="col-md-4" >
          <div class="ibox">
               <div class="ibox-content" style="text-align: center;">
                    <?php if($examen['tiempo']): ?>
                    <h2>Tiempo restante</h2>
                    <script>
                    function alerta() {
                         $('#myModal').modal('show');
                         window.setInterval( 'submit()' , 5000);
                    }
                    function submit() {
                         $( "#regForm" ).submit()
                    }
                    var myCountdown2 = new Countdown({time: {{ $examen['tiempo'] }},
                                                       width:200,
                                                       height:80,
                                                       hideLine: 1,
                                                       rangeHi:"hour",	// <- no comma on last item!
                                                       numbers : {
                                                                      font : "Arial",
                                                                      color : "#FFFFFF",
                                                                      bkgd	: "#005a95",
                                                                      rounded : 0.15,
                                                                      shadow : {
                                                                                x : 0,
                                                                                y : 3,
                                                                                s : 4,
                                                                                c : "#000000",
                                                                                a : 0.4
                                                                      }
                                                                 },
                                                       labels : {
                                                           font   : "Arial",
                                                           color  : "#a8a8a8",
                                                            textScale 	: 0.8,
                                                           weight : "normal" // < - no comma on last item!
                                                       },
                                                       labelText	: {
                                                            ms    	: "MS",
                                                            second : "SEGUNDOS",
                                                            minute : "MINUTOS",
                                                            hour  	: "HORAS"
                                                       },
                                                       onComplete : alerta
                                                       });


                    </script>
                    <style> div#Stage_jbeeb_3{  margin: 0 auto !important; } </style>
                    <?php endif;?>
                    <div style="text-align:center;margin-top:40px;">
                         @foreach ($preguntas as $key => $pregunta)
                         <span class="step">{{ $key +1 }}</span>
                         @endforeach
                    </div>
               </div>
          </div>
     </div>
</div>


<div id="myModal" class="modal fade" data-backdrop="static"> role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body" style="text-align: center;">
           <div class="row">
             <img src="https://i.pinimg.com/originals/7a/05/1f/7a051f327179adcd3d5d1c19644aa7f2.png" alt="reloj" style="height:200px; width:200px;">
             <h2>El timpo se ha terminado</h2>
           </div>
      </div>
    </div>

  </div>
</div>

@endsection
