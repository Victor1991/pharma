

               <div class="panel-group" id="accordion">
                    @foreach ($secciones as $key => $seccion)
                    <div class="panel panel-default shadow">
                         <div class="panel-heading c_azul">
                              <h4 class="panel-title">
                                   <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}"
                                   {{ $key == '0' ? ' class="panel-body collapse " aria-expanded="true"' : ''  }}>
                                   {{ $seccion['info']['titulo'] }}</a>
                              </h4>
                         </div>
                         <?php if (isset($material['seccion_id'])): ?>
                              <div id="collapse{{$key}}" class="panel-body collapse <?php if($seccion['info']['id'] == $material['seccion_id'] ):?> in <?php endif;?> ">
                         <?php else: ?>
                              <div id="collapse{{$key}}" class="panel-body collapse <?php if($key == 0 ):?> in <?php endif;?> ">
                         <?php endif; ?>


                              <div class="panel-body">
                                   @foreach ($seccion['materiales'] as $key2 => $material_info)
                                   @php
                                   /* variables a utilizar */
                                   $color = '';
                                   $color_fin = 'color:#3382d2;';
                                   $url = '#';

                                   /* materiales terminados */
                                   $terminados =  array_column($curso['porcentaje'][3], 'material_id');

                                   /* material en el que se encuentra */
                                   if($material['id'] == $material_info['id']) { $color = 'text-success'; } ;

                                   /* marcar materiales finalizados */
                                   if (in_array( $material_info['id'], $terminados)) {
                                        $color_fin = 'text-info';
                                        $url = url('/ver_material/'.$material_info['id']);
                                   }

                                   if ($curso['porcentaje'][4][0] == $material_info['id']){
                                        $color_fin = 'text-warning';
                                        $url = url('/ver_material/'.$material_info['id']);
                                   }


                                   @endphp
                                   <div class="forum-item">
                                        <div class="row">
                                             <div class="col-md-10">
                                                  <div class="forum-icon">
                                                       <a href="{{ $url }}">
                                                            @if ($material_info['tipo_archivo']  == "specialkey" )
                                                            <i class="fa fa-list-alt {{$color}} " aria-hidden="true" style="color:#3382d2;"></i>
                                                            @elseif($material_info['tipo_archivo'] == "video")
                                                            <i class="fa fa-play-circle-o {{$color}} " style="color:#3382d2;"></i>
                                                            @elseif($material_info['tipo_archivo'] == "pdf")
                                                            <i class="fa fa-file-pdf-o {{$color}} " style="color:#3382d2;"></i>
                                                            @endif
                                                       </a>
                                                  </div>
                                                  <ol class="breadcrumb" style="font-size: 20px; font-weight: 200">
                                                       <li>
                                                            <a href="{{ $url }}"  >
                                                                 <strong>{{ $material_info['titulo'] }}</strong><br>
                                                                 {!! $material_info['doctores'] !!}<br>
                                                                 {{ $material_info['duracion'] ? 'Duración: '. $material_info['duracion'] : '' }}
                                                                 <?php
                                                                 $correctas = $material_info->respuestas_correctas($material_info['id']);
                                                                 if ($correctas['type'] == true) :
                                                                 ?>
                                                                      <br>
                                                                      Aciertos {{ $correctas['response'] }} de {{ $material_info->preguntas->count() }}
                                                                 <?php endif; ?>
                                                            </a>
                                                       </li>
                                                  </ol>
                                                  <!--<div class="forum-sub-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>-->
                                             </div>
                                             <div class="col-md-1 forum-info m-t-sm">
                                                  <span class="views-number ">
                                                       {{ $material_info['caracteristicas'] }}
                                                  </span>
                                             </div>
                                             <div class="col-md-1 forum-info  m-t-md ">
                                                  <span class="views-number">
                                                       <i style="{{ $color_fin }}" class="fa fa-play  " ></i>
                                                  </span>
                                             </div>
                                        </div>
                                   </div>

                                   @endforeach
                              </div>
                         </div>
                    </div>
                    <br>
                    @endforeach
               </div>
     <div style="margin-bottom:20px;"></div>
