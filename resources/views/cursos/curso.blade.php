@extends('layouts.app')

@section('title', 'Main page')

@section('content')

<!-- Page heading -->

<style>
    .head_curso{
        background: url({!! asset('img/'.$curso["imagen"]) !!}) no-repeat center center fixed;
         -webkit-background-size: cover;
         -moz-background-size: cover;
         -o-background-size: cover;
         background-size: cover;
    }
</style>




<section id="content">

    <div class="container clearfix">


        <div class="jumbotron " style=" background-image: url({!! asset('img/'.$curso['imagen']) !!});  padding-top:40px;padding-bottom:40px;background-repeat:no-repeat;background-position:center;background-size:cover;-webkit-background-size:cover; border-radius: 10px;" >
            <div class="row">
                <div class="col-md-8">

                    <span class="tit1" style="color:#{{ $curso['color_principal'] }}">{!! $curso['nombre'] !!}</span><br>

                 <p style="font-size: 16px; color: #{{ $curso['color_principal'] }}; text-align: justify;"><?php echo $curso['descripcion'] ?></p>
                </div>
                <div class="col-md-4 ">
                    <img src="{!! asset('img/'.$curso['imagen_patrocinador']) !!}"  class="center-block " style="border-radius: 6px;">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <span style="color: #8d8d8d; font-size: 16px;">Completado: </span>
                    <small class="pull-right" style="color: #8d8d8d; font-size: 16px;">{{ $curso['porcentaje'][2]}} Módulos de  {{ $curso['porcentaje'][1]}}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {{ $curso['porcentaje'][0] }}%;" class="progress-bar"></div>
                </div>
            </div>

        </div>



        <div class="row">
            <div class="col-lg-12">






                <div class="tabs tabs-bb clearfix" id="tab-9" style="border: 0px;">

                    <ul class="tab-nav clearfix">
                        <li ><a href="#tabs-33">Descripción</a></li>
                        <li onclick="option_video('pause')"><a href="#tabs-34">Instrucciones</a></li>
                        <li onclick="option_video('pause')"><a href="#tabs-35">Módulos</a></li>

                        <?php if(count($medicos_participantes) > 0){ ?>
                             <li onclick="option_video('pause')">
                                   <a href="#tabs-36">Profesores Participantes</a>
                              </li>
                        <?php } ?>
                        <?php if( count($recursos_relacionados) > 0){?>
                             <li onclick="option_video('pause')">
                                   <a href="#tabs-37">Recursos Relacionados</a>
                              </li>
                        <?php } ?>
                    </ul>

                    <div class="tab-container" style="border: 0px; margin-top: 25px;">
                        <div class="clearfix" id="tabs-33" style="border: 0px;">
                            @foreach ($material_introductorio as $key_mat => $material_intro)
                            <div class="row">
                                   <div class="col-md-6">
                                   @if($material_intro['tipo_archivo'] == 'video')
                                        <video controls controlsList="nodownload" preload="auto" controls style="display: block; width: 100%;">
                                             <source src='{{ asset('material/pdf/').'/'.$material_intro['archivo']  }}' type='video/mp4' >
                                        </video>
                                   @else
                                        <div class="embed-responsive embed-responsive-16by9">
                                             <iframe class="embed-responsive-item" src="{{ asset('material/pdf/').'/'.$material_intro['archivo']  }}" allowfullscreen></iframe>
                                        </div>
                                   @endif
                                   </div>
                                   <div class="col-md-6">
                                        <h3 style="color:#3ba0ff; margin: 0 0 3px 0;">{{ $material_intro['titulo'] }}</h3>
                                        <h4  style="color:#3ba0ff; font-weight: 100">{!! $material_intro['descripcion'] !!}</h4>
                                   </div>
                            </div>
                            <br>
                            @endforeach
                            <?php echo $curso['objetivo']; ?>
                        </div>
                        <div class="tab-content clearfix" id="tabs-34"  style="border: 0px;">
                              {!! $curso['instrucciones']  !!}
                        </div>
                        <div class="tab-content clearfix" id="tabs-35"  style="border: 0px; min-height: 1120px;">
                            @include('cursos.secciones')
                        </div>

                        <div class="tab-content clearfix" id="tabs-36"  style="border: 0px;">
                            <div class="row">
                                   @foreach ($medicos_participantes as $key_mat => $medico)
                                        <div class="col-md-6 col-lg-6">
                                             <div class="slide" style=" margin-bottom: 30px;">
                                                  <div class="testi-image">
                                                       <img src="{!! asset('img').'/'.$medico['foto'] !!}" alt="">
                                                  </div>
                                                  <div class="testi-content">
                                                       <p style="line-height: 1.2;">
                                                            {{ $medico['nombre'] }}
                                                       </p>
                                                      <div class="testi-meta">
                                                          <span>
                                                                 <?php echo $medico['descripcion']; ?>
                                                          </span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                   @endforeach
                            </div>
                        </div>

                         <?php if( count($recursos_relacionados) > 0){?>
                         <div class="tab-content clearfix" id="tabs-37"  style="border: 0px;">
                              <H3  style="color: #556c98;  font-weight: 200; margin: 0 0 20px 0;">Recursos Relacionados</h3>
                                   <div class="row">
                                        @foreach ($recursos_relacionados as $key_mat => $rec_rel)
                                             <div class="col-md-6 col-lg-6">
                                                  <div class="iportfolio">
                                                       <div class="portfolio-image">
                                                            <video poster="images/videos/explore.jpg" preload="auto" controls style="display: block; width: 100%;">
                                                                 <source src='{{ asset('material/relacionados/').'/'.$rec_rel['archivo']  }}' type='video/mp4' />
                                                            </video>
                                                       </div>
                                                       <div class="portfolio-desc" style=" font-weight: 200; ">
                                                            <h2 style=" color: #009cde;  font-weight: 200; margin: 0 0 10px 0; ">
                                                                 {{ $rec_rel['nombre'] }}
                                                            </h2>
                                                           <h4 style="  font-weight: 200; ">
                                                                 {{ $rec_rel['descripcion'] }}
                                                           </h4>
                                                       </div>
                                                  </div>
                                             </div>
                                        @endforeach
                                   </div>
                              </div>
                         <?php } ?>
                    </div>

                </div>

                <center>
                    <?php if (is_null($ultimo_intento['fin'])): ?>
                         <?php if ($ultim_curso == 'fin'): ?>
                              <?php if($examen['id']): ?>
                                   <a href="{{url('/examen/'.$examen['id'])}}" class="btn btn-primary btn-lg active" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;">Realizar examen <i class=" fa fa-arrow-circle-right" style="color:#fff;"></i> </a>
                              <?php endif; ?>
                         <?php endif; ?>
                    <?php else: ?>
                              <?php if ($ultimo_intento['num_intento'] <  $curso['num_intentos'] && $ultimo_intento['aprobado'] == 0): ?>
                              <a href="{{url('/reiniciar_curso/'.$curso['id'])}}" class="btn btn-primary btn-lg active" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;">Reiniciar curso <i class=" fa fa-arrow-circle-right" style="color:#fff;"></i> </a>
                              <?php endif; ?>

                    <?php endif; ?>



                    <?php if ($ultim_curso != 'fin'): ?>

                         <a href="{{url('/ver_material/'.$ultim_curso[0])}}" class="btn btn-primary btn-lg active" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;">Continuar con el curso <i class=" fa fa-arrow-circle-right" style="color:#fff;"></i> </a>
                    <?php endif; ?>
                </center>

            </div>
        </div>


        <div class="row">
            <div class="col-md-12 text-center" style=" margin-top: 30px;"  >

                <h6 style="color: #005a8f; font-weight: 200">Todo el material de este sitio web está protegido por derechos de autor, Copyright EduPlace © 2019.<br>
                    Este sitio web también contiene material con derechos de autor de terceros.</h6>
            </div>
        </div>


    </div>






</section>

@endsection

@push('scripts')
<script>

function option_video(option){
     if(option == 'play'){
           $('video').get(0).play();
     }else{
           $('video').get(0).pause();
     }
}
</script>
@endpush
