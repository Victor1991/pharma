@extends('layouts.app')

@section('content')

<style media="screen">
.container input {
     opacity: 1;
     position: relative;
}
</style>

<script type="text/javascript">
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57)
}
</script>

<div class="wrapper wrapper-content animated fadeInRight container">
     <div class="row">

          <div class="col-lg-12">
               <div class="ibox ">
                    <div class="ibox-title">
                         <h5>Editar Perfil</h5>
                    </div>

                    <div class="container m-t-md">
                        @if(session('message'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ session('message') }}</strong>
                        </div>
                        @endif

                        @if(session('error'))
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> > {{ session('error') }} </strong>
                        </div>
                        @endif
                    </div>


                    <div class="ibox-content">
                         <form class="form-horizontal" method="POST" action="{{ route('editar_data_perfil', $id)  }}">
                              {{ csrf_field() }}
                              <div class="row">

                                   <div class="col-md-6">


                                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">

                                             <div class="col-md-12">
                                                  <label for="nombre"> Nombre :</label>
                                                  <input id="nombre" placeholder="Nombre" type="text" class="form-control" name="nombre" value="{{ $usuario->nombre or old('nombre') }}" required autofocus>

                                                  @if ($errors->has('nombre'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('nombre') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('apellido_paterno') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="nombre"> Apellido Paterno :</label>
                                                  <input id="apellido_paterno" placeholder="Apellido Paterno*" type="text" class="form-control " name="apellido_paterno" value="{{  $usuario->apellido_paterno or old('apellido_paterno') }}" required autofocus>
                                                  @if ($errors->has('apellido_paterno'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('apellido_paterno') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('apellidos_materno') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="nombre"> Apellido Materno :</label>
                                                  <input id="apellidos_materno" placeholder="Apellido Materno*" type="text" class="form-control " name="apellidos_materno" value="{{ $usuario->apellidos_materno or old('apellidos_materno') }}" required autofocus>
                                                  @if ($errors->has('apellidos_materno'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('apellidos_materno') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('apellidos_materno') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="nombre"> Sexo :</label>
                                                  <select class="form-control "  name="sexo" id="sexo" required autofocus>
                                                       <option value="" selected disabled hidden>Sexo *</option>
                                                       <option {{ (  $usuario->sexo  ==  'masculino') ? 'selected' : '' }} value="masculino" > Masculino</option>
                                                       <option {{ (   $usuario->sexo ==  'femenino') ? 'selected' : '' }} value="femenino" > Femenino </option>
                                                  </select>
                                                  @if ($errors->has('sexo'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('sexo') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('edad') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="edad"> Edad :</label>
                                                  <input id="edad" placeholder="Edad *" onKeyPress="return soloNumeros(event)" type="text" class="form-control" name="edad" value="{{  $usuario->edad or old('edad') }}"  maxlength="2" required autofocus>
                                                  @if ($errors->has('edad'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('edad') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="pais"> País :</label>
                                                  <select class="form-control" name="pais" id="pais" required autofocus >
                                                       <option value="" selected disabled hidden>País *</option>
                                                       @foreach ($pais as $pais)
                                                       <option {{ ( $usuario->pais_id == $pais['id']) ? 'selected' : '' }} value="{{ $pais['id'] }}" > {{ $pais['nombre'] }}</option>
                                                       @endforeach
                                                  </select>
                                                  @if ($errors->has('pais'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('pais') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('ciudad') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="pais"> Ciudad :</label>
                                                  <input id="ciudad" placeholder="Ciudad " type="text" class="form-control" name="ciudad" value="{{ $usuario->ciudad or old('ciudad') }}" >
                                                  @if ($errors->has('ciudad'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('ciudad') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('codigo_postal') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="codigo_postal"> C.P. :</label>
                                                  <input id="codigo_postal" placeholder="Codigo Postal " type="text" class="form-control" name="codigo_postal" value="{{ $usuario->cp or old('codigo_postal') }}">
                                                  @if ($errors->has('codigo_postal'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('codigo_postal') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>





                                   </div>

                                   <div class="col-md-6">

                                        <div class="form-group{{ $errors->has('cedula_profecional') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="cedula_profecional"> Cedula Profesional :</label>
                                                  <input id="cedula_profecional" placeholder="Cedula Profesional* " type="text" class="form-control" name="cedula_profecional" value="{{ $usuario->cedula or  old('cedula_profecional') }}" required autofocus>
                                                  @if ($errors->has('cedula_profecional'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('cedula_profecional') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('especialidad') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="cedula_profecional"> Especialidad :</label>
                                                  <select class="form-control" name="especialidad" id="especialidad" required autofocus >
                                                       <option value="" selected disabled hidden>Especialidad *</option>
                                                       @foreach ($especialidades as $especialidad)
                                                       <option {{ ( $usuario->especialidad_id  == $especialidad['id']) ? 'selected' : '' }} value="{{ $especialidad['id'] }}" > {{ $especialidad['nombre'] }}</option>
                                                       @endforeach
                                                  </select>
                                             </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('institucion') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <!-- <input id="institucion" placeholder="Institución* " type="text" class="form-control" name="institucion" value="{{ old('institucion') }}" required autofocus> -->
                                                  <label for="institucion">  Institución :</label>
                                                  <select class="form-control" name="institucion" id="institucion" required autofocus >
                                                       <option value="" selected disabled hidden> Institución* </option>
                                                       @foreach ($instituciones as $instituciones)
                                                       <option {{ ( $usuario->institucion_id  == $instituciones['id']) ? 'selected' : '' }} value="{{ $instituciones['id'] }}" > {{ $instituciones['nombre'] }}</option>
                                                       @endforeach
                                                  </select>
                                                  @if ($errors->has('institucion'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('institucion') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="celular">  Número de celular :</label>

                                                  <input id="celular" placeholder="Número de celular* " type="text" class="form-control" name="celular" value="{{  $usuario->celular or old('celular') }}" required autofocus>
                                                  @if ($errors->has('celular'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('celular') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                             <div class="col-md-12">
                                                  <label for="password"> Password :</label>

                                                  <input id="password" type="password" class="form-control" name="password" placeholder="password">

                                                  @if ($errors->has('password'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('password') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                                             <div class="col-md-12">
                                                  <label for="password"> Confirmar password :</label>

                                                  <input type="password" class="form-control" name="password_confirmation" placeholder="confirmar password">

                                                  @if ($errors->has('password_confirmation'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>




                                        <div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
                                             <div class="col-md-12">
                                                  <label for="correo"> Correo :</label>
                                                  <input id="correo" type="correo" class="form-control" name="correo" value="{{ $usuario->correo or old('correo') }}"  required>
                                                  @if ($errors->has('correo'))
                                                  <span class="help-block">
                                                       <strong>{{ $errors->first('correo') }}</strong>
                                                  </span>
                                                  @endif
                                             </div>
                                        </div>




                                   </div>

                              </div>
                              <hr>


                              <div class="row">
                                   <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary btn-block">
                                             Guardar
                                        </button>
                                   </div>
                                   <div class="col-md-6">
                                        <a href="{{ url('/home') }}" class="btn btn-outline btn-default btn-block">
                                             Regresar
                                        </a>
                                   </div>
                              </div>

                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>


@endsection
