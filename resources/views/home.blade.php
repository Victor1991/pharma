@extends('layouts.app')
@section('title', 'Main page')
@section('content')

<style>
     .mc{
          height: 485px;
     }
     .text-overlay{
          padding: 15px 15px 15px;
     }
</style>


<!-- Page heading -->


<div class="jumbotron" style=" background-image: url({!! asset('img/bg121.png') !!});  padding-top:40px;padding-bottom:40px;background-repeat:no-repeat;background-position:center;background-size:cover;-webkit-background-size:cover; border-radius: 0px;" >
    <div class="row">
        <div class="col-md-6 text-center">

            <span class="tit1">Bienvenido a</span><br>
            <img src="{!! asset('img/logo2.png') !!}">
            <p style="font-size: 28px; color: #fff;">Ahora formas parte<br>
                de la comunidad médica informada</p>
        </div>
    </div>
</div>


<div class="container m-t-md">
    @if(session('message'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> ¡Felicidades! </strong> {{ session('message') }}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> ¡Lo sentimos! </strong> {{ session('error') }}
    </div>
    @endif
</div>


<section id="content">



    <div class="container clearfix">



        <div class="tabs tabs-bb clearfix" id="tab-9" style="border: 0px;">

            <ul class="tab-nav clearfix">
                <li><a href="#tabs-33">MIS CURSOS</a></li>
                <!--
                <li><a href="#tabs-34">Iniciados</a></li>
                <li><a href="#tabs-35">Sin iniciar</a></li>
                <li ><a href="#tabs-36">Terminados</a></li>
                <li ><a href="#tabs-36">Mis constancias</a></li>
                -->
            </ul>

            <div class="tab-container" style="border: 0px;">

                <div class="clearfix" id="tabs-33" style="border: 0px;">
                     <div class="row">
                          @foreach($cursos as $curso)
                               @if($curso['activo'] == 1)
                               <div class="col-md-4 mc oc-item">
                             <div class="iportfolio">
                                 <div class="portfolio-image " >

                                         <img src="{!! asset('img/'.$curso['imagen_small']) !!}" class="img-rounded "  >

                                         <div class="text-overlay img-rounded " style="">

                                             <div >

                                                 <!-- <h4 class="" style="color: #{{ $curso['color_principal'] }}; font-weight: bold" >{{ $curso['num_modulos']}} MÓDULOS</h4> -->
                                                 <p style="color: #{{ $curso['color_principal'] }}; font-size: 18px; line-height: 20px; font-weight: 100; margin-bottom: -5px;">{!! $curso['nombre'] !!} </p><br><br>
                                                      <?php if ($curso['text_sec_pres']): ?>
                                                           <p style="color: #{{ $curso['color_principal'] }}; font-size: 10px; line-height: 10px; font-weight: 100">
                                                                {!! $curso['text_sec_pres'] !!} <br><br>
                                                           </p>
                                                      <?php endif; ?>



                                             </div>

                                         </div>



                                 </div>



                                 <div class="portfolio-desc" style="margin-right: 0px">

                               <div class="row">
                                    <div class="col-xs-6">

                                         @if($curso['ultimo_material'][2] == "inicio" && $curso['aprobado'] != 1)

                                         <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Iniciar curso</a>

                                         @elseif($curso['ultimo_material'][2] == 'continuar')

                                         <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Continuar</a>



                                         @else

                                              <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-green btn-sm">Ver curso</a>


                                              <?php if (is_null($curso['ultimo_intento']['fin'])) : ?>
                                                   <?php if ($curso['examen']['id'] && $curso['ultimo_intento']['aprobado'] == 0): ?>
                                                        <a type="button" href="{{ url('/examen/'.$curso['examen']['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Examen </a>
                                                   <?php endif; ?>


                                              <?php endif;?>

                                              <?php if ($curso['ultimo_intento']['aprobado'] == 1 && $curso['certificado'] == 1): ?>
                                                   <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="_blank" class="button  button-small button-circle button-border button-blue btn-sm">Ver tu certificado</a>
                                              <?php endif; ?>


                                         @endif

                                    </div>

                                    <div class="col-xs-6" style=" text-align: right;">
                                         <span style="font-size: 13px; margin-top:11px;"><?=$curso['porcentaje'][0]?>% completado</span>
                                    </div>
                               </div>

                                 </div>


                             </div>
                         </div>
                               @endif


                         @endforeach
                     </div>

                    <!-- <div id="oc-portfolio"  data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="3" data-items-lg="3">





                    </div> -->




                </div>


                <!--
                <div class="tab-content clearfix" id="tabs-34"  style="border: 0px;">
                    Próximamente
                </div>
                <div class="tab-content clearfix" id="tabs-35"  style="border: 0px;">
                    Próximamente
                </div>
                <div class="tab-content clearfix" id="tabs-36"  style="border: 0px;">
                    Próximamente
                </div>
                -->

            </div>

        </div>


     <!--
        <HR>



        <h4 style="color: #556c98;  ">NUEVOS CURSOS</h4>


        <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="3" data-items-lg="3">


            @foreach($cursos as $curso)
            <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image " >
                        <a href="portfolio-single.html">
                            <img src="{!! asset('img/'.$curso['imagen']) !!}" class="img-rounded "  >

                            <div class="text-overlay img-rounded " style="">

                                <div >

                                    <h4 class="" style="color: #fff; font-weight: bold" >{{ $curso['porcentaje'][1]}} MÓDULOS</h4>
                                    <p style="color: #fff; font-size: 18px; line-height: 20px; font-weight: 100">{{ $curso['nombre']}} <br><br>


                                    </p>
                                </div>

                            </div>

                        </a>

                    </div>
                    <div class="portfolio-desc" style="margin-right: 0px">

                        <span style="font-size: 12px; ">100% completado</span>


                        @if($curso['ultimo_material'][2] == 'inicio')

                        <a type="button" href="{{ url('/ver_material/'.$curso['ultimo_material'][0]) }}" class="btn btn-outline btn-success btn-block">Iniciar curso</a>

                        @elseif($curso['ultimo_material'][2] == 'continuar')

                        <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="btn btn-outline btn-success btn-block">Continuar</a>

                        @elseif($curso['aprobado'] == 1)

                        <p class="text-info">Felicidades por aprobar el curso</p>
                        <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="_blank" class="btn btn-outline btn-success btn-block">Ver tu certificado</a>

                        @else

                        <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="btn btn-outline btn-success btn-block">Ver cursos</a>


                        <a type="button" href="{{ url('/examen/'.$curso['id']) }}" class="btn btn-outline btn-info btn-block">Examen <i class="fa fa-dot-circle-o"></i> </a>

                        @endif



                    </div>

                </div>
            </div>



            @endforeach


        </div>






        <hr>


        <h     4 style="color: #556c98;"> CURSOS POPULARES</h4>


        <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="3" data-items-lg="3">


            @foreach($cursos as $curso)
            <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image " >
                        <a href="portfolio-single.html">
                            <img src="{!! asset('img/'.$curso['imagen']) !!}" class="img-rounded "  >

                            <div class="text-overlay img-rounded " style="">

                                <div >

                                    <h4 class="" style="color: #fff; font-weight: bold" >{{ $curso['porcentaje'][1]}} MÓDULOS</h4>
                                    <p style="color: #fff; font-size: 18px; line-height: 20px; font-weight: 100">{{ $curso['nombre']}} <br><br>


                                    </p>
                                </div>

                            </div>

                        </a>

                    </div>
                    <div class="portfolio-desc" style="margin-right: 0px">

                        <span style="font-size: 12px; ">100% completado</span>


                        @if($curso['ultimo_material'][2] == 'inicio')

                        <a type="button" href="{{ url('/ver_material/'.$curso['ultimo_material'][0]) }}" class="btn btn-outline btn-success btn-block">Iniciar curso</a>

                        @elseif($curso['ultimo_material'][2] == 'continuar')

                        <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="btn btn-outline btn-success btn-block">Continuar</a>

                        @elseif($curso['aprobado'] == 1)

                        <p class="text-info">Felicidades por aprobar el curso</p>
                        <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="_blank" class="btn btn-outline btn-success btn-block">Ver tu certificado</a>

                        @else

                        <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="btn btn-outline btn-success btn-block">Ver cursos</a>


                        <a type="button" href="{{ url('/examen/'.$curso['id']) }}" class="btn btn-outline btn-info btn-block">Examen <i class="fa fa-dot-circle-o"></i> </a>

                        @endif



                    </div>

                </div>
            </div>



            @endforeach


        </div>
          -->




          <!--
          <center><h1 style="color: #556c98;  margin: 0 0 0px 0;"> EXPERTOS</h1>
            <H3  style="color: #556c98;  font-weight: 200; margin: 0 0 40px 0;">Aprendiendo de los más reconocidos.</h3></center>



        <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="3" data-items-lg="3">



            <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image">

                            <img src="{!! asset('img/esp2.png') !!}" alt="">


                    </div>
                    <div class="portfolio-desc" style="background-color: #52678c; font-weight: 200; ">
                        <h4 style="color: #fff; margin-left: 20px; font-weight: 200; ">Dra. María Gabriela Liceaga Craviotto</h4>
                        <h5 style="color: #fff; margin-left: 18px; font-weight: 200; ">Médico especialista en Medicina Interna<br><br>
                            Hospital Dr. Carlos McGregor Sánchez IMSS<br>
                        </h5>

                    </div>
                </div>
            </div>
            <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image">

                            <img src="{!! asset('img/esp1.jpg') !!}" alt="">


                    </div>
                    <div class="portfolio-desc" style="background-color: #52678c; font-weight: 200; ">
                        <h4 style="color: #fff; margin-left: 20px; font-weight: 200; ">Dra. Fátima Higuera de la Tijera</h4>
                        <h5 style="color: #fff; margin-left: 18px; font-weight: 200; ">Médico especialista en Gastroenterología y Hepatología<br>
                        Hospital General de México<br>
                        </h5>
                    </div>
                </div>
            </div>




            <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image">

                            <img src="{!! asset('img/esp3.jpg') !!}" alt="">

                    </div>
                    <div class="portfolio-desc" style="background-color: #52678c; font-weight: 200; ">
                        <h4 style="color: #fff; margin-left: 20px; font-weight: 200; ">Dra. Eira Cerda Reyes</h4>
                        <h5 style="color: #fff; margin-left: 18px; font-weight: 200; ">Médico especialista en Gastroenterología y Hepatología<br>
                        Hospital Central Militar<br>
                        </h5>
                    </div>
                </div>
            </div>
              <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image">

                            <img src="{!! asset('img/esp4.jpg') !!}" alt="">

                    </div>
                    <div class="portfolio-desc" style="background-color: #52678c; font-weight: 200; ">
                        <h4 style="color: #fff; margin-left: 20px; font-weight: 200; ">Dra. Nayelli Cointa Flores Garcia </h4>
                        <h5 style="color: #fff; margin-left: 18px; font-weight: 200; ">Médico especialista en Gastroenterología y Hepatología<br>
                        Instituto Nacional de Ciencias Médicas y Nutrición “Salvador Zubirán”
                        </h5>
                    </div>
                </div>
            </div>
              <div class="oc-item">
                <div class="iportfolio">
                    <div class="portfolio-image">

                            <img src="{!! asset('img/esp5.jpg') !!}" alt="">

                    </div>
                    <div class="portfolio-desc" style="background-color: #52678c; font-weight: 200; ">
                        <h4 style="color: #fff; margin-left: 20px; font-weight: 200; ">Dr. Mauricio Castillo Barradas </h4>
                        <h5 style="color: #fff; margin-left: 18px; font-weight: 200; ">Médico especialista en Gastroenterología y Hepatología<br>
                        Centro Médico Nacional “La Raza” IMSS<br>
                        </h5>
                    </div>
                </div>
            </div>




        </div>
          -->



    </div></section>


<!--

<div class="wrapper wrapper-content animated fadeInRight container">
    <div class="row">
        @foreach($cursos as $curso)
        <div class="col-md-6">
            <div class="ibox ">
                <div class="ibox-content borde-negro">
                    <div class="row">
                        <div class="col-md-5">
                            <img src="{!! asset('img/'.$curso['imagen']) !!}" alt="img_curso" class="img-responsive borde-negro">
                        </div>
                        <div class="col-md-7">
                            <h2>{{ $curso['nombre']}}</h2>
                            <h4>{{ $curso['porcentaje'][2]}} de {{ $curso['porcentaje'][1]}} Materiales</h4>
                            <h3><strong>{{ $curso['cliente']['empresa'] }}</strong></h3>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <span>Completado</span>
                                <small class="pull-right">{{ $curso['porcentaje'][0] }}%</small>
                            </div>
                            <div class="progress progress-small">
                                <div style="width: {{ $curso['porcentaje'][0] }}%;" class="progress-bar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if($curso['ultimo_material'][2] == 'inicio')
                        <div class="col-md-6">
                            <a type="button" href="{{ url('/ver_material/'.$curso['ultimo_material'][0]) }}" class="btn btn-outline btn-success btn-block">Iniciar curso</a>
                        </div>
                        @elseif($curso['ultimo_material'][2] == 'continuar')
                        <div class="col-md-6">
                            <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="btn btn-outline btn-success btn-block">Continuar</a>
                        </div>
                        @elseif($curso['aprobado'] == 1)
                        <div class="col-md-12 text-center">
                            <p class="text-info">Felicidades por aprobar el curso</p>
                            <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="_blank" class="btn btn-outline btn-success btn-block">Ver tu certificado</a>
                        </div>
                        @else
                        <div class="col-md-6">
                            <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="btn btn-outline btn-success btn-block">Ver cursos</a>
                        </div>
                        <div class="col-md-6">
                            <a type="button" href="{{ url('/examen/'.$curso['id']) }}" class="btn btn-outline btn-info btn-block">Examen <i class="fa fa-dot-circle-o"></i> </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


-->
@endsection
