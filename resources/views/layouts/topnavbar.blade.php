






<!--<div class="row border-bottom white-bg">
     <nav class="navbar navbar-static-top container " role="navigation">
          <div class="navbar-header">
               <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
               </button>
               <img src="{!! asset('img/be_logo.png') !!}" alt="logo" class="img-nav"/>
          </div>
          <div class="navbar-collapse collapse" id="navbar">
               <ul class="nav navbar-top-links navbar-right">
                   <li class="dropdown">
                         {{ Auth::user()->name }}
                         <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">

                              <img src="{!! asset('img/logo_perfil.png') !!}" class="img-perfil" alt="perfil">
                         </a>
                         <ul class="dropdown-menu dropdown-alerts">
                              <li>
                                   <a href="{{ route('editar_alumno') }}">
                                        Editar perfil
                                   </a>
                              </li>
                              <li>
                                   <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar sesion
                                   </a>
                              </li>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                   {{ csrf_field() }}
                              </form>
                         </ul>
                    </li>
                    <li>
                         <a href="{{ url('/') }}">MIS CURSOS</a>
                    </li>
                    <li class="dropdown" style="display:none;">
                         <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                              <i class="fa fa-bell"></i>  <span class="label label-primary">1</span>
                         </a>
                         <ul class="dropdown-menu dropdown-alerts">
                              <li>
                                   <a href="mailbox.html">
                                        <div>
                                             <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                             <span class="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                   </a>
                              </li>

                         </ul>
                    </li>

               </ul>
          </div>
     </nav>
</div>
-->

<header id="header">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="" class="standard-logo" data-dark-logo="{!! asset('img/be_logo.png') !!}"><img src="{!! asset('img/be_logo.png') !!}" alt=""></a>
                <a href="" class="retina-logo" data-dark-logo="{!! asset('img/be_logo.png') !!}"><img src="{!! asset('img/be_logo.png') !!}" alt=""></a>
            </div><!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="style-2">

                <ul>
                    <li class="dropdown">

                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">

                            {{ Auth::user()->nombre }}  {{ Auth::user()->apellido_paterno }}  {{ Auth::user()->apellido_materno }}<img src="{!! asset('img/logo_perfil.png') !!}" class="img-perfil" alt="perfil">
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">

                         <li>
                                     <a href="{{ route('editar_alumno') }}">
                                             Editar perfil
                                     </a>
                             </li>
                           
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                    Cerrar sesion
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </ul>
                    </li>
                    <li><a href="{{ url('/home') }}" style="color: #676a6c; font-weight: bold"><div> MIS CURSOS</div></a></li>
                    <!--<li><a href="#" style="color: #676a6c;" ><div> AYUDA</div></a></li>-->

                </ul>



            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header><!-- #header end -->
