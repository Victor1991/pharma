<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'eduPlace') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />



     <link rel="stylesheet" href="{!! asset('css/bootstrap.css') !!}" type="text/css" />
	<link rel="stylesheet" href="{!! asset('css/style.css') !!}" type="text/css" />
	<link rel="stylesheet" href="{!! asset('css/swiper.css') !!}" type="text/css" />

	<link rel="stylesheet" href="{!! asset('css/font-icons.css') !!}" type="text/css" />
	<link rel="stylesheet" href="{!! asset('css/animate.css') !!}" type="text/css" />

     <link rel="stylesheet" href="{!! asset('css/magnific-popup.css') !!}" type="text/css" />

	<link rel="stylesheet" href="{!! asset('css/responsive.css') !!}" type="text/css" />
     <link rel="stylesheet" href="{!! asset('css/estilos/estilos.css') !!}" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />





    <!-- Sweet alert  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script src="{!! asset('js/countdown.js') !!}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.js" type="text/javascript"></script>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155648111-1"></script>
    <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-155648111-1');
     </script>

</head>
<body class="stretched">
     <div id="load_img" style="width: 100%;
                   height: 100%;
                   position: fixed;
                   z-index: 999999999999999;
                   background-color: #f7fbfdbd;
                   text-align: center;
                    display: none;
                    ">
               <img style="margin-top: 20%;" src="{!! asset('spinner.gif') !!}" alt="">
     </div>
  <!-- Wrapper-->
    <div id="wrapper" class="clearfix">

        <!-- Navigation -->
        <!-- @include('layouts.navigation') -->

        <!-- Page wraper -->


            <!-- Page wrapper -->
            @include('layouts.topnavbar')


            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts.footer')


        <!-- End page wrapper-->



    </div>
    <!-- End wrapper-->



    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>

    <!-- Sweet alert  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>


     <!-- JS ADD -->
     <script src="{!! asset('js/script.js') !!}" type="text/javascript"></script>


        <script type="text/javascript" src="{!! asset('js/jquery.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('js/plugins.js') !!}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="{!! asset('js/functions.js') !!}"></script>

     @section('scripts')
     @show
     @stack('scripts')

</body>
</html>
