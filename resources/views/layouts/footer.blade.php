
<footer id="footer" class="dark">

    <div class="container">


        <div class="footer-widgets-wrap clearfix">


            <div class="col-md-8 ">
                <!--
                <a href="#" class="btn-social btn-linkedin" style="margin-right: 10px;">
                    <i class="icon-linkedin "></i></a>
                <a href="#" class="btn-social btn-twitter" style="margin-right: 10px;">
                    <i class="icon-twitter "></i></a>
                <a href="#" class="btn-social btn-facebook" style="margin-right: 10px;">
                    <i class="icon-facebook "></i></a>
                <a href="#" class="btn-social btn-youtube" style="margin-right: 10px;">
                    <i class="icon-youtube "></i></a>
                <a href="#" class="btn-social btn-vimeo" style="margin-right: 10px;">
                    <i class="icon-vimeo "></i></a>
                -->
                <br><br>

                <img src="{!! asset('img/logb.png') !!}" ><br>
                <span style="color:#335987; font-size: 16px;">
                    © 2019 eduPlace. Todos los derechos reservados
                </span>
            </div>
            <div class="col-md-2" style="font-size: 14px; color: #fff">


<!--
                <h4 style="font-size: 20px;line-height: 29px; font-weight: 200; letter-spacing: 0px; color: #4d6184; margin: 0 0 20px 0;">CONECTAR</h4>
                Blog<br>
                Facebook<br>
                LinkedIn<br>
                Twitter<br>
                Youtube<br>
-->
            </div>
            <div class="col-md-2" style="font-size: 14px; color: #fff">



                <h4 style="font-size: 20px;line-height: 29px; font-weight: 200; letter-spacing: 0px; color: #4d6184; margin: 0 0 20px 0;">MÁS</h4>
                <a href="{!! asset('material/pdf/aviso_privacidad_eduplace.pdf') !!}" target="_blank" style="color:#fff">Aviso de Privacidad</a><br>
                <a href="{!! asset('material/pdf/legales_eduplace.pdf') !!}" target="_blank" style="color:#fff">Legales</a><br>
                Contacto<br>
                <a href="mailto:contacto@eduplace.com.mx">contacto@eduplace.com.mx</a><br>

            </div>

        </div>

    </div>



</footer>
