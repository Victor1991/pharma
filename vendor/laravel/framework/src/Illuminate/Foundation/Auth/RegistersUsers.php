<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;


use App\Especialidades;
use App\Alumnos_cursos;
use App\Pais;
use App\Residentes;
use App\Instituciones;



trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $data['curso_id'] = $request->curso_id;
        $data['correo'] = $request->correo;
        $data['especialidades'] = Especialidades::orderBy('nombre')->get()->toArray();
        $data['pais'] = Pais::orderBy('nombre')->get()->toArray();
        $data['residentes'] = Residentes::orderBy('nombre')->get()->toArray();
        $data['instituciones'] = Instituciones::orderBy('nombre')->get()->toArray();

        return view('auth.register', $data );
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

    //      dump($_POST);
    // die;
    //

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                    ? : redirect($this->redirectPath());
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(){
         return redirect()->route('registro_exitoso');
    }
}
